package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;

import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.ItemTouchHelperAdapter;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ProductTypeFragment;

/**
 * Created by Vishesh on 04-01-2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.DataObjectHolder> implements ItemTouchHelperAdapter
{

    private final Context context;
    private int currentposition;
    private boolean flag;
    private final ProductTypeFragment productTypeFragment;


    private boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }


    public int getCurrentPosition() {
        return currentposition;
    }

    public void setCurrentPosition(int currentposition) {
        this.currentposition = currentposition;
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

// --Commented out by Inspection START (28/11/16 11:51 AM):
//        public TextView gettDisplayText() {
//            return tDisplayText;
//        }
// --Commented out by Inspection STOP (28/11/16 11:51 AM)

        public final TextView tDisplayText,tDisplayStatusText;
        public final RelativeLayout rlProductType;
        final MyListener callback;


        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            tDisplayText = (TextView) itemView.findViewById(R.id.tDisplayText);
            tDisplayStatusText = (TextView) itemView.findViewById(R.id.tDisplayStatusText);
            rlProductType = (RelativeLayout) itemView.findViewById(R.id.rlProductType);

            callback = (MyListener) context;

            itemView.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View v) {

            DashboardActivity.fab.setVisibility(View.GONE);

            if (getCurrentPosition() == getAdapterPosition() && !isFlag()) {
                rlProductType.setBackgroundColor(Color.LTGRAY);
                Master.backCheck=1;
                setCurrentPosition(getAdapterPosition());
                productTypeFragment.editInvisible(true);
                setFlag(true);

            } else if(getCurrentPosition() == getAdapterPosition() && isFlag()){
                rlProductType.setBackgroundColor(Color.WHITE);
                Master.backCheck=0;
                productTypeFragment.editInvisible(false);
                DashboardActivity.fab.setVisibility(View.VISIBLE);
                setFlag(false);

            } else if(getCurrentPosition() != getAdapterPosition() && !isFlag()){
                rlProductType.setBackgroundColor(Color.LTGRAY);
                setCurrentPosition(getAdapterPosition());
                Master.backCheck=1;
                productTypeFragment.editInvisible(true);
                setFlag(true);

            }



            return true;
        }
    }



    public RecyclerViewAdapter(Context context, Fragment productTypeFragment) {
        this.context = context;
        this.productTypeFragment=(ProductTypeFragment) productTypeFragment;
        setFlag(false);

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new DataObjectHolder(view, context);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {

        holder.tDisplayText.setText(Master.productTypeDisplayList.get(position).getName());

        if(Master.productTypeDisplayList.get(position).productTypeStatus==1)
        {
            holder.tDisplayStatusText.setText(context.getResources().getString(R.string.label_textview_enable));

            holder.tDisplayStatusText.setTextColor(Color.GREEN);
        }
        else
        {
            holder.tDisplayStatusText.setText(context.getResources().getString(R.string.label_textview_disable));

            holder.tDisplayStatusText.setTextColor(Color.RED);

        }

        if(position==getCurrentPosition() && isFlag()){
            holder.rlProductType.setBackgroundColor(Color.LTGRAY);
        }
        else
            holder.rlProductType.setBackgroundColor(Color.WHITE);


    }

    @Override
    public int getItemCount() {
        return Master.productTypeDisplayList.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition<getItemCount() && toPosition<getItemCount()) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(Master.productTypeDisplayList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {

                    Collections.swap(Master.productTypeDisplayList, i, i - 1);

                }
            }
            notifyItemMoved(fromPosition, toPosition);
        }
    }

    @Override
    public void onItemDismiss(int position) {
        if(position<getItemCount()+1) {
            Master.productTypeDisplayList.remove(position);
            notifyItemRemoved(position);
        }
    }

}
