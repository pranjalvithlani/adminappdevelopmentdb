package admin.lokacart.ict.mobile.com.adminapp.util;

/**
 * Created by Vishesh on 24-12-2015.
 */

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.io.OutputStreamWriter;
        import java.io.UnsupportedEncodingException;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Iterator;
        import java.util.List;
        import java.util.Map;

        import org.apache.http.client.ClientProtocolException;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import android.util.Base64;


public class GetJSON {

    private static GetJSON getJSON;

    public static GetJSON getInstance() {

        if (getJSON == null) {
            getJSON = new GetJSON();
        }
        return getJSON;
    }

    private GetJSON() {
    }




    public String getJSONFromUrl(String url,JSONObject obj,String method,boolean auth,String emailid,String password) {

        String status;

        //noinspection deprecation,deprecation,deprecation,deprecation,deprecation,deprecation,deprecation
        InputStream is;
        HttpURLConnection linkConnection;
        //noinspection deprecation
        try {

            URL linkurl = new URL(url);

            linkConnection = (HttpURLConnection) linkurl.openConnection();

            if(auth) {
                String basicAuth = "Basic " + new String(Base64.encode((emailid+":"+password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
            }

            linkConnection.setDefaultUseCaches(false);

            if(method.equals("GET"))
            {
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "application/json");
                linkConnection.setDoInput(true);
            }

            if(method.equals("POST"))
            {
                linkConnection.setRequestMethod("POST");
                linkConnection.setRequestProperty("Content-Type", "application/json");
                linkConnection.setRequestProperty("Accept", "application/json");
                linkConnection.setDoOutput(true);
                linkConnection.setDoInput(true);

                String o = obj.toString();

                OutputStreamWriter w = new OutputStreamWriter(linkConnection.getOutputStream());
                w.write(o);
                w.flush();
                w.close();
            }


            status =String.valueOf(linkConnection.getResponseCode());



            if(status.equals("200"))
            {
                is = linkConnection.getInputStream();
            }
            else
            { status ="exception";
                return status;
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            status ="exception";
            return status;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            status ="exception";
            return status;
        } catch (IOException e) {
            e.printStackTrace();
            status ="exception";
            return status;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            status = sb.toString();

            try {
                new JSONObject(status);
                return status;
            } catch (Exception e) {
                status ="exception";
                return status;
            }


        } catch (Exception ignored) {


        }
        finally {
            linkConnection.disconnect();
        }

        return status;
    }
    private static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }





    private static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }




}
