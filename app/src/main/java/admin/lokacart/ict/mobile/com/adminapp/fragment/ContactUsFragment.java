package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 7/6/16.
 */
public class ContactUsFragment extends DialogFragment implements View.OnClickListener {


    private long mLastClickTime = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_us,container,false);
        TextView faqTV = (TextView)view.findViewById(R.id.faq_string_TV);

        ImageView facebookIV = (ImageView) view.findViewById(R.id.facebook_iv);
        ImageView callUsIV = (ImageView) view.findViewById(R.id.call_us_iv);
        ImageView websiteIV = (ImageView) view.findViewById(R.id.website_iv);
        ImageView mailUsIV = (ImageView) view.findViewById(R.id.mail_us_iv);



        facebookIV.setOnClickListener(this);
        callUsIV.setOnClickListener(this);
        websiteIV.setOnClickListener(this);
        mailUsIV.setOnClickListener(this);

        faqTV.setOnClickListener(this);
        Button closeDialogFragmentTV = (Button) view.findViewById(R.id.close_dailof_fragment_tv);

        closeDialogFragmentTV.setOnClickListener(this);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }



    @Override
    public void onClick(View view) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch(view.getId()){

            case R.id.call_us_iv:{
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+918291586240"));
                startActivity(intent);
                break;

            }
            case R.id.mail_us_iv:{
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","lokacart@cse.iitb.ac.in", null));

                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;

            }
            case R.id.website_iv:{
                String url = "http://ruralict.cse.iitb.ac.in/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            }
            case R.id.facebook_iv:{
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/RuralICT.iitb/"));
                startActivity(i);
                break;

            }

            case R.id.close_dailof_fragment_tv:{
                getDialog().dismiss();
                break;

            }

            case R.id.faq_string_TV:{
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FAQFragment(), Master.FAQ_TAG).commit();
                getDialog().dismiss();
                break;

            }
        }
    }



}



