package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 4/6/16.
 */
public class TermsAndConditionsFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View termsAndConditionsFragmentView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        getActivity().setTitle(R.string.title_fragment_terms_and_conditions);
        setHasOptionsMenu(true);

        if(TermsAndConditionsFragment.this.isAdded() && getActivity()!=null)
        ((DashboardActivity)getActivity()).updateStatusBarColor();

        return termsAndConditionsFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
