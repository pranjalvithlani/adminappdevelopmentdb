package admin.lokacart.ict.mobile.com.adminapp.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vishesh on 08-02-2016.
 */
public class Member
{
    private int userID;
    private String name;
    private String lastname;
    private String address;
    private String email;
    private String phone;
    private String pincode;
    private String role;
    private boolean isA;
    private boolean isP;
    private boolean isM;

    public Member(JSONObject object)
    {
        assign(object);
        try {
            userID = object.getInt("userID");
        } catch (JSONException ignored) {
        }
        try {
            role = object.getString("role");
        } catch (JSONException ignored) {
        }
        isA = role.toLowerCase().contains("admin");
        isP = role.toLowerCase().contains("publisher");
        isM = role.toLowerCase().contains("member");
    }

    public Member(JSONObject jsonObject, int i)
    {
        //while adding a new member
        assign(jsonObject);
        try
        {
            isA = !jsonObject.getString("isAdmin").equals("0");

            isP = !jsonObject.getString("isPublisher").equals("0");

            isM = true;
        }
        catch (Exception ignored)
        {
        }

    }

    private void assign(JSONObject object)
    {
        try {
            name = object.getString("name");
        } catch (JSONException ignored) {
        }
        try {
            address = object.getString("address");
        } catch (JSONException ignored) {
        }
        try {
            email = object.getString("email");
        } catch (JSONException ignored) {
        }
        try {
            phone = object.getString("phone").substring(2);
        } catch (JSONException ignored) {
        }

        try {
            pincode = object.getString("pincode");
        }
        catch (Exception e) {
            pincode = null;
        }
        try{
            role = object.getString("role");
        }
        catch (Exception e){
            role = null;
        }
        try {
            lastname = object.getString("lastname");
        }
        catch (Exception e) {
            lastname = null;
        }
    }


// --Commented out by Inspection START (28/11/16 11:56 AM):
// --Commented out by Inspection START (28/11/16 11:57 AM):
////   public void setName(String name)
////   {
////        this.name = name;
////   }
//// --Commented out by Inspection STOP (28/11/16 11:56 AM)
//   public void setRole(String role){
//        this.role = role;
//   }
// --Commented out by Inspection STOP (28/11/16 11:57 AM)
    public void setAddress(String address)
    {
       this.address = address;
    }
// --Commented out by Inspection START (28/11/16 11:57 AM):
// --Commented out by Inspection START (28/11/16 11:57 AM):
////    public void setEmail(String email)
////    {
////        this.email = email;
////   }
//// --Commented out by Inspection STOP (28/11/16 11:57 AM)
//   public void setPhone(String phone)
//    {
//        this.phone = phone;
//    }
// --Commented out by Inspection STOP (28/11/16 11:57 AM)
// --Commented out by Inspection START (28/11/16 11:57 AM):
    public void setUserID(int userID)
    {
        this.userID = userID;
    }
// --Commented out by Inspection STOP (28/11/16 11:57 AM)
    public void setPincode(String pincode)
    {
        this.pincode = pincode;
    }

// --Commented out by Inspection START (28/11/16 11:58 AM):
// --Commented out by Inspection START (28/11/16 11:58 AM):
////    public void setA(boolean isA)
////   {
////      this.isA = isA;
////    }
//// --Commented out by Inspection STOP (28/11/16 11:58 AM)
//    public void setP(boolean isP)
//    {
//        this.isA = isP;
//    }
// --Commented out by Inspection STOP (28/11/16 11:58 AM)
    public void setLastname(String lastName)
    {
        this.lastname = lastName;
    }

    public String getName()
    {
        return name;
    }
    public String getRole() {
        return role;
    }
    public String getAddress()
    {
        return address;
    }
    public String getEmail()
    {
        return email;
    }
    public String getPhone()
    {
        return phone;
    }
    public String getUserID()
    {
        return userID + "";
    }
    public String getLastName() { return lastname; }
    public int getIntUserID()
    {
        return userID;
    }
    public String getPincode()
    {
        return pincode;
    }

// --Commented out by Inspection START (28/11/16 11:58 AM):
//    public boolean isA()
//    {
//       return isA;
//    }
// --Commented out by Inspection STOP (28/11/16 11:58 AM)

// --Commented out by Inspection START (28/11/16 11:58 AM):
//    public boolean isP()
//    {
//        return isP;
//    }
// --Commented out by Inspection STOP (28/11/16 11:58 AM)

// --Commented out by Inspection START (28/11/16 11:59 AM):
//public boolean isM()
//    {
//        return isM;
//    }
// --Commented out by Inspection STOP (28/11/16 11:59 AM)
}
