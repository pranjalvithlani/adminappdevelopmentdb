package admin.lokacart.ict.mobile.com.adminapp.interfaces;

/**
 * Created by Vishesh on 25-01-2016.
 */
public interface NavigationItemListener {
     void itemSelected(int itemIndex);
}
