package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.DataObjectHolder>
{
    private final ArrayList<String> productList;
    private final ArrayList<String> productQuantityList;
    private final ArrayList<String> productPriceList;
    private final ArrayList<String> productStatusList;
    private final Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        final TextView tProductName;
        final TextView tProductQuantity;
        final TextView tProductPrice;
        final TextView tOutOfStock;
        final TextView tProductStatus;
        public DataObjectHolder(View itemView)
        {
            super(itemView);
            tProductName = (TextView) itemView.findViewById(R.id.tProductName);
            tProductPrice = (TextView) itemView.findViewById(R.id.tProductPrice);
            tProductQuantity = (TextView) itemView.findViewById(R.id.tProductQuantity);
            tOutOfStock = (TextView) itemView.findViewById(R.id.tOutOfStock);
            tProductStatus = (TextView) itemView.findViewById(R.id.tProductStatus);
        }
    }

    public ProductRecyclerViewAdapter(ArrayList<String> myDataset,ArrayList<String> quantity,ArrayList<String> price,ArrayList<String> status, Context context)
    {
        productList = myDataset;
        productQuantityList = quantity;
        productPriceList = price;
        productStatusList=status;
        this.context = context;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card_view, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        holder.tProductName.setText(productList.get(position));
        holder.tProductPrice.setText(productPriceList.get(position));
        holder.tProductQuantity.setText(productQuantityList.get(position));
        if(productStatusList.get(position).equals("0")){


            holder.tProductStatus.setText(context.getResources().getString(R.string.label_textview_disable));
            holder.tProductStatus.setTextColor(Color.RED);
        }
        else {
            holder.tProductStatus.setTextColor(Color.GREEN);
            holder.tProductStatus.setText(context.getResources().getString(R.string.label_textview_enable));
        }

        if(productQuantityList.get(position).equals("0") || productQuantityList.get(position).equals("0.0"))
        {
            holder.tOutOfStock.setVisibility(View.VISIBLE);
        }
        else
            holder.tOutOfStock.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
