package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.SwipeRefreshLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.util.Member;
import admin.lokacart.ict.mobile.com.adminapp.adapter.MemberRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.adapter.RecyclerItemClickListener;

/**
 * Created by Vishesh on 08-02-2016.
 */
public class ExistingUserFragment extends Fragment {


    private ProgressDialog pd;
    private static ArrayList<Member> memberArrayList;
    private ArrayList<JSONObject> memberObjects;
    private SwipeRefreshLayout swipeContainer;
    private RecyclerView mRecyclerView;
    private static MemberRecyclerViewAdapter mAdapter;
    private JSONObject responseObject;
    private static int recyclerViewIndex;
    private View existingUserFragmentView;
    private Dialog dialog;
    private String memberName;
    private String memberLName;
    private String memberEmail;
    private String memberAddress;
    private String memberPhone;
    private String memberPincode;
    private final int ADD_KEY = 0, EDIT_KEY = 1;
    private int count = 0;
    private TextView tMember;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        existingUserFragmentView = inflater.inflate(R.layout.fragment_member_recycler_view, container, false);
        getActivity().setTitle(R.string.title_members);
        setHasOptionsMenu(true);
        tMember = (TextView) existingUserFragmentView.findViewById(R.id.tMember);
        tMember.setText(R.string.label_no_member_present);
        tMember.setVisibility(View.GONE);
        memberArrayList = new ArrayList<>();
        swipeContainer = (SwipeRefreshLayout) existingUserFragmentView.findViewById(R.id.memberSwipeToRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getActivity()))
                    new GetExistingMembersTask(false).execute();
                else
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);
        return existingUserFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private ArrayList<Member> getMembers() {

        ArrayList<Member> members = new ArrayList<>();
        if(memberObjects.size()!=0)
        {
            for(JSONObject entry : memberObjects){
                String role = null;
                try {
                    role = entry.getString("role");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(role.equals("Member")) {
                    Member member = new Member(entry);
                    members.add(member);
                }

            }
        }

        return members;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) existingUserFragmentView.findViewById(R.id.memberRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        if(Master.isNetworkAvailable(getActivity()))
            new GetExistingMembersTask(true).execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

    }


    public void addNewMember()
    {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.member_box);
        dialog.setTitle(R.string.dialog_title_add_member);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eName, eLName, eEmail, ePhone, eAddress, ePincode;
        final Button bEdit, bConfirm, bCancel,bDelete;
        final CheckBox cAdmin, cPublisher, cMember;
        final TextView tRole;

        tRole = (TextView) dialog.findViewById(R.id.tRole);
        tRole.setVisibility(View.GONE);

        eName = (EditText) dialog.findViewById(R.id.eMemberFirstName);
        eLName = (EditText) dialog.findViewById(R.id.eMemberLastName);
        eEmail = (EditText) dialog.findViewById(R.id.eMemberEmail);
        ePhone = (EditText) dialog.findViewById(R.id.eMemberPhone);
        eAddress = (EditText) dialog.findViewById(R.id.eMemberAddress);
        ePincode = (EditText) dialog.findViewById(R.id.eMemberPincode);

        cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        cAdmin.setVisibility(View.GONE);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        cPublisher.setVisibility(View.GONE);

        cMember = (CheckBox) dialog.findViewById(R.id.cMember);
        cMember.setVisibility(View.GONE);

        bEdit = (Button) dialog.findViewById(R.id.bMemberEdit);
        bEdit.setVisibility(View.GONE);

        bDelete=(Button) dialog.findViewById(R.id.bMemberDelete);
        bDelete.setVisibility(View.GONE);

        bConfirm = (Button) dialog.findViewById(R.id.bMemberConfirm);
        bCancel = (Button) dialog.findViewById(R.id.bMemberCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //TODO send the new product name
                memberName = eName.getText().toString().trim();
                memberLName = eLName.getText().toString().trim();
                memberPhone = ePhone.getText().toString().trim();
                memberEmail = eEmail.getText().toString().trim();
                memberAddress = eAddress.getText().toString().trim();
                memberPincode = ePincode.getText().toString().trim();

                if (memberPhone.equals(""))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_all_fields_are_mandatory, Toast.LENGTH_SHORT).show();
                }
                else if(!memberEmail.isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(memberEmail).matches())
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_email_id, Toast.LENGTH_SHORT).show();
                }
                else if(memberPhone.length() != 10)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_mobile_number, Toast.LENGTH_SHORT).show();
                }
                else if(memberPincode.length() != 6 && memberPincode.length() != 0)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_pincode, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Member tempMember;
                    JSONObject jsonObject = new JSONObject();
                    try
                    {
                        jsonObject.put("phonenumber", "91" + memberPhone);
                        if(!memberEmail.equals("")) {
                            jsonObject.put("email", memberEmail);
                        }
                        jsonObject.put("abbr", AdminDetails.getAbbr());
                        jsonObject.put("refemail",AdminDetails.getEmail());
                        jsonObject.put("address", memberAddress);
                        jsonObject.put("name",memberName);
                        jsonObject.put("lastname", memberLName);
                        jsonObject.put("pincode", memberPincode);
                        tempMember = new Member(jsonObject, 0);
                        if(Master.isNetworkAvailable(getActivity()))
                            new AddEditMemberTask(tempMember, dialog, ADD_KEY, 0).execute(jsonObject);
                        else
                            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void clickListener(final int position)
    {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.member_box);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eName, eLName, eEmail, ePhone, eAddress, ePincode;
        final Button bEdit, bConfirm, bCancel,bDelete;
        final CheckBox cAdmin, cPublisher, cMember;
        final TextView tRole;

        tRole = (TextView) dialog.findViewById(R.id.tRole);
        tRole.setVisibility(View.GONE);

        eName = (EditText) dialog.findViewById(R.id.eMemberFirstName);
        eName.setText(memberArrayList.get(position).getName());
        eName.setEnabled(false);

        eLName = (EditText) dialog.findViewById(R.id.eMemberLastName);
        eLName.setText(memberArrayList.get(position).getLastName());
        eLName.setEnabled(false);

        eEmail = (EditText) dialog.findViewById(R.id.eMemberEmail);
        eEmail.setText(memberArrayList.get(position).getEmail());
        eEmail.setEnabled(false);

        ePhone = (EditText) dialog.findViewById(R.id.eMemberPhone);
        ePhone.setText(memberArrayList.get(position).getPhone());
        ePhone.setEnabled(false);

        eAddress = (EditText) dialog.findViewById(R.id.eMemberAddress);
        eAddress.setText(memberArrayList.get(position).getAddress());
        eAddress.setEnabled(false);

        ePincode = (EditText) dialog.findViewById(R.id.eMemberPincode);
        ePincode.setText(memberArrayList.get(position).getPincode());
        ePincode.setEnabled(false);

        cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        cAdmin.setVisibility(View.GONE);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        cPublisher.setVisibility(View.GONE);

        cMember = (CheckBox) dialog.findViewById(R.id.cMember);
        cMember.setVisibility(View.GONE);


        bEdit = (Button) dialog.findViewById(R.id.bMemberEdit);
        bConfirm = (Button) dialog.findViewById(R.id.bMemberConfirm);
        bConfirm.setVisibility(View.GONE);

        bDelete=(Button) dialog.findViewById(R.id.bMemberDelete);

        bCancel = (Button) dialog.findViewById(R.id.bMemberCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
                alertdialog.setTitle(getString(R.string.label_button_delete)+ " " + memberArrayList.get(position).getName() + " ?");

                alertdialog.setPositiveButton(getString(R.string.label_button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                alertdialog.setNegativeButton(getString(R.string.label_button_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // make the delete API call over here
                        if(getActivity()!=null && Master.isNetworkAvailable(getActivity())){
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("abbr",AdminDetails.getAbbr());
                                jsonObject.put("phonenumber", "91" + memberArrayList.get(position).getPhone());

                                new DeleteMember(position).execute(jsonObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                        }else{
                            Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                alertdialog.show();

            }
        });

        bEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eName.setEnabled(true);
                eLName.setEnabled(true);
                eAddress.setEnabled(true);
                cPublisher.setEnabled(true);
                cAdmin.setEnabled(true);
                bEdit.setVisibility(View.GONE);
                bConfirm.setVisibility(View.VISIBLE);
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //TODO send the new product name
                memberName = eName.getText().toString().trim();
                memberLName = eLName.getText().toString().trim();
                memberPhone = ePhone.getText().toString().trim();
                memberEmail = eEmail.getText().toString().trim();
                memberAddress = eAddress.getText().toString().trim();
                memberPincode = ePincode.getText().toString().trim();

                if (memberName.equals("") || memberLName.equals("") || memberAddress.equals("")|| memberEmail.equals("") || memberPhone.equals("") || memberPincode.equals(""))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_all_fields_are_mandatory, Toast.LENGTH_SHORT).show();
                }
                else if(memberPhone.length() != 10)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_mobile_number, Toast.LENGTH_SHORT).show();
                }
                else if(!memberEmail.equals("") && !android.util.Patterns.EMAIL_ADDRESS.matcher(memberEmail).matches())
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_email_id, Toast.LENGTH_SHORT).show();
                }
                else if(memberPincode.length() != 6)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_pincode, Toast.LENGTH_SHORT).show();
                }
                else if(memberName.equals(memberArrayList.get(position).getName())
                        && memberLName.equals(memberArrayList.get(position).getLastName())
                        && memberEmail.equals(memberArrayList.get(position).getEmail())
                        && memberPhone.equals(memberArrayList.get(position).getPhone())
                        && memberAddress.equals(memberArrayList.get(position).getAddress())
                        && memberPincode.equals(memberArrayList.get(position).getPincode()))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                }
                else if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                {
                    Member tempMember;
                    JSONObject jsonObject = new JSONObject();
                    try
                    {
                        jsonObject.put("name", memberName);
                        jsonObject.put("lastname", memberLName);
                        jsonObject.put("userid", memberArrayList.get(position).getUserID());
                        jsonObject.put("email", memberEmail);
                        jsonObject.put("phone", "91" + memberPhone);
                        jsonObject.put("address", memberAddress);
                        jsonObject.put("pincode", memberPincode);
                        tempMember = new Member(jsonObject, 0);
                        tempMember.setPincode(memberPincode);
                        tempMember.setUserID(memberArrayList.get(position).getIntUserID());
                        new AddEditMemberTask(tempMember, dialog, EDIT_KEY, position).execute(jsonObject);
                    }

                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                else{
                    Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public class GetExistingMembersTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        final Boolean showProgressDialog;

        public GetExistingMembersTask(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getExistingUsersURL(AdminDetails.getAbbr()), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String message)
        {
            if(showProgressDialog)
            {
                if(pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if(ExistingUserFragment.this.isAdded()){
                if (message.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try
                    {

                        memberObjects= new ArrayList<>();
                        responseObject = new JSONObject(message);
                        JSONArray jsonArray = responseObject.getJSONArray("users");
                        if(jsonArray.length() == 0)
                        {
                            tMember.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            count++;
                            tMember.setVisibility(View.GONE);
                            for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                            {
                                memberObjects.add((JSONObject)jsonArray.get(recyclerViewIndex));
                            }
                            memberArrayList = getMembers();

                            mAdapter = new MemberRecyclerViewAdapter(memberArrayList, getActivity());
                            if(count < 2)
                            {
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.existingUserClickKey, ExistingUserFragment.this));
                                mAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                mRecyclerView.swapAdapter(mAdapter,true);
                            }
                        }
                        swipeContainer.setRefreshing(false);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }

    private class AddEditMemberTask extends AsyncTask<JSONObject, String, String>
    {
        final Member member;
        final Dialog d;
        final int category;
        int position;
        public AddEditMemberTask(Member member, Dialog dialog, int category, int position)
        {
            d = dialog;
            this.member = member;
            if(category == EDIT_KEY)
            {
                this.position = position;
                this.category = category;
            }
            else
                this.category = ADD_KEY;
        }
        @Override
        protected void onPreExecute()
        {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();


            if(category == ADD_KEY)
                return getJSON.getJSONFromUrl(Master.getAddUserURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            else
                return getJSON.getJSONFromUrl(Master.getEditUserURL(AdminDetails.getAbbr()), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());

        }

        @Override
        protected void onPostExecute(String message)
        {

            if(pd != null && pd.isShowing()){
                pd.dismiss();
            }


            if(ExistingUserFragment.this.isAdded()){
                if(message.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try
                    {
                        responseObject = new JSONObject(message);
                        if(responseObject.getString("response").equals("success"))
                        {
                            dialog.dismiss();

                            if(category == ADD_KEY)
                            {
                                Toast.makeText(getActivity(), R.string.label_toast_member_added_successfully, Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                memberArrayList.set(position, member);
                                mAdapter.notifyItemChanged(position);
                                mAdapter.notifyDataSetChanged();
                                Toast.makeText(getActivity(), R.string.label_toast_member_details_successfully_updated, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if(responseObject.getString("response").equals("Email ID already exists"))
                        {
                            Toast.makeText(getActivity(), R.string.label_toast_email_id_already_exists, Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("Phone Number already exists"))
                        {
                            Toast.makeText(getActivity(), R.string.label_toast_phone_number_already_exists, Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("Already a member")){
                            Toast.makeText(getActivity(),R.string.label_toast_already_a_member,Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("failure")){

                            try{
                                if(responseObject.getString("reason").equals("Admin accounts cannot be used on the consumer app")){
                                    Toast.makeText(getActivity(),R.string.label_toast_admin_acc_not_on_client_app,Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();

                            }
                        }
                        else if(responseObject.getString("response").equals("Email ID and Phone number mismatch")){


                            Toast.makeText(getActivity(), R.string.label_toast_email_phone_mismatch, Toast.LENGTH_SHORT).show();

                        }
                        else if(responseObject.getString("response").equals("User have not registered the actual eMail ID. Try removing the eMail")){
                            Toast.makeText(getActivity(), R.string.label_toast_wrong_email_id, Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                            //Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }


        }
    }

    private  class DeleteMember extends AsyncTask<JSONObject,String,String >{

        ProgressDialog pd;
        final int position;

        public DeleteMember(int position){
            this.position = position;

        }

        @Override
        protected void onPreExecute() {
            pd=new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getDeleteUser(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd != null && pd.isShowing()){
                pd.dismiss();
            }

            if(response.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
            }
            else
            {

                try
                {
                    responseObject = new JSONObject(response);
                    response = responseObject.getString("response");
                    if (response.equals("Cannot be deleted")) {
                        Toast.makeText(getActivity(), R.string.label_toast_cannot_delete_member, Toast.LENGTH_SHORT).show();
                    } else if (response.equals("Membership removed")) {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), R.string.label_toast_member_removed, Toast.LENGTH_SHORT).show();
                        memberArrayList.remove(position);
                        mAdapter.notifyItemRemoved(position);
                        recyclerViewIndex--;
                    }
                    else{
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                }


            }

        }


    }

}

