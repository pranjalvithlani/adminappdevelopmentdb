package admin.lokacart.ict.mobile.com.adminapp.util;
/**
 * Created by shyam on 3/10/16.
 */

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import admin.lokacart.ict.mobile.com.adminapp.R;

public class UploadImageDisplay extends AppCompatActivity {

    private ImageView ivProduct;
    private String abspath,productId;
    private String imgPath;
    private String productName;
    private String tag;
    private File file;
    // --Commented out by Inspection (28/11/16 11:52 AM):File file1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_upload_image_display);
        setContentView(R.layout.image_box);
        ivProduct = (ImageView) findViewById(R.id.ivProduct);
        //TextView imagePath = (TextView) findViewById(R.id.tImageLink);
        //Button bSave = (Button) findViewById(R.id.bImageUpload);
        Button bCancel = (Button) findViewById(R.id.bCancel);
        //Random randomno = new Random();
        //Bundle b = new Bundle();

        String orgImgPath = getIntent().getStringExtra("imgpath");
        tag = getIntent().getStringExtra("TAG");
        if(tag.equals("EditProductActivity")){
            productName = getIntent().getStringExtra("productname");
            productId = getIntent().getStringExtra("id");
        }
        final Bitmap bitmap = rotateImage(orgImgPath);


        Button bSave = (Button) findViewById(R.id.bImageUpload);
        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Master.isNetworkAvailable(UploadImageDisplay.this))
                {
                    Toast.makeText(UploadImageDisplay.this,R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    new UploadImageTask(abspath,productId, bitmap).execute();
                }

            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                finish();
                try {
                    //file1.delete();
                    Master.clearBitmap(bitmap);
                } catch (Exception ignored) {

                }
            }
        });
        ivProduct.setImageBitmap(bitmap);
        ivProduct.setVisibility(View.VISIBLE);


        //new UploadImageTask(abspath, bitmap,false).execute();



        /*byte[] byteArray = getIntent().getByteArrayExtra("img");
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imgv.setImageBitmap(bmp);
        tv.setText(getIntent().getStringExtra("imgPath"));*/

    }




    private Bitmap rotateImage(final String path) {
        final Bitmap[] b = new Bitmap[1];
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                b[0] = decodeFileFromPath(path);
                try {
                    ExifInterface ei = new ExifInterface(path);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            System.out.println("Case 90   "+ExifInterface.ORIENTATION_ROTATE_90);
                            matrix.postRotate(90);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            System.out.println("Case 90   "+ExifInterface.ORIENTATION_ROTATE_180);
                            matrix.postRotate(180);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            System.out.println("Case 90   "+ExifInterface.ORIENTATION_ROTATE_270);
                            matrix.postRotate(270);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        default:
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }

                FileOutputStream out1 = null;
                File file;
                try {
                    String state = Environment.getExternalStorageState();
                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".jpg");
                    }
                    else {
                        file = new File(getFilesDir() , "image" + new Date().getTime() + ".jpg");
                    }
                    out1 = new FileOutputStream(file);
                    b[0].compress(Bitmap.CompressFormat.JPEG, 90, out1);
                    //ivProduct.setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
                    abspath = file.getAbsolutePath();
                    ivProduct.setImageBitmap(b[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        out1.close();
                    } catch (Throwable ignore) {

                    }
                }

            }
        });
        return b[0];
    }

    private Bitmap decodeFileFromPath(String path){
        Uri uri = getImageUri(path);
        InputStream in = null;
        try {
            try {
                in = getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(in, null, o);
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


            int scale = 1;
            int inSampleSize = 1024;
            if (o.outHeight > inSampleSize || o.outWidth > inSampleSize) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(inSampleSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = getContentResolver().openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            return b;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
    public String getAbsolutePath(Uri uri) {
        if(Build.VERSION.SDK_INT >= 19){
            String id = uri.getLastPathSegment().split(":")[1];
            final String[] imageColumns = {MediaStore.Images.Media.DATA };
            final String imageOrderBy = null;
            Uri tempUri = getUri();
            Cursor imageCursor = getContentResolver().query(tempUri, imageColumns,MediaStore.Images.Media._ID + "="+id, null, imageOrderBy);
            if (imageCursor.moveToFirst()) {
                return imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }else{
                return null;
            }
        }else{
            String[] projection = { MediaStore.MediaColumns.DATA };
            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else
                return null;
        }

    }

    */

   /* private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if(!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }*/

    private Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }
/*
    public Uri setImageUri() {

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".jpg");
            Uri imgUri = Uri.fromFile(file);
            this.imgPath = file.getAbsolutePath();
            return imgUri;
        }
        else {
            File file = new File(getFilesDir() , "image" + new Date().getTime() + ".jpg");
            Uri imgUri = Uri.fromFile(file);
            this.imgPath = file.getAbsolutePath();
            return imgUri;
        }
    }

    public String getImagePath() {
        return imgPath;
    }
    */




    //----------------------Async task to upload image------------------------
    class UploadImageTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        final String path;
        //DialogPlus dialog;
        final Bitmap bitmap;
        final Boolean isCaptured;
        final String id;
        // --Commented out by Inspection (28/11/16 11:52 AM):String imagePath;

        UploadImageTask(String path,String id, Bitmap bitmap)
        {
            this.bitmap = bitmap;
            this.path = path;
            //dialog = dialogPlus;
            this.isCaptured= false;
            this.id=id;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(UploadImageDisplay.this);
            pd.setMessage(getString(R.string.pd_uploading_file));
            pd.setCancelable(false);
            pd.show();

            // System.out.println(path);

            file = new File(path);

            //System.out.println(file.getAbsolutePath());
            // System.out.println(file.getParent());

            /*imagePath = file.getParent() + "/" + "myImage" + randomno.nextInt(1000000) + ".jpg";


            file1 = new File(imagePath);
            try
            {
                Master.copyFile(file, file1);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            if(tag.equals("EditProductActivity")) {
                response = Master.okhttpUpload(file, Master.getUploadImageURL(productId),
                        AdminDetails.getEmail(), AdminDetails.getPassword());
            }
            else{
                response = Master.okhttpUpload(file, Master.getGeneralSettingsLogoUploadURL(AdminDetails.getAbbr()),
                        AdminDetails.getEmail(), AdminDetails.getPassword());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject responseObject = new JSONObject(message);
                message=responseObject.optString("response");
                //System.out.println("message"+message.toString());

                switch (message) {
                    case "Image upload successful":
                        Toast.makeText(UploadImageDisplay.this,
                                R.string.label_toast_image_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                        Master.clearBitmap(bitmap);
                        // make finish instead of dialog dismiss();
                        //dialog.dismiss();
                        finish();
                        break;
                    case "timeout":
                        Toast.makeText(UploadImageDisplay.this, "Timeout uploading image", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(UploadImageDisplay.this,R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                        break;
                }
            }
            catch(JSONException e){
                Toast.makeText(UploadImageDisplay.this, R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
            }
           /* catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(UploadImageDisplay.this,R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
            }*/
        }
    }
    //----------------Async task end-----------------------------------

}
