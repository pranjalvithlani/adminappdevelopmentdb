package admin.lokacart.ict.mobile.com.adminapp.adapter;

/**
 * Created by Vishesh on 19-01-2016.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import admin.lokacart.ict.mobile.com.adminapp.fragment.BillLayoutFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.DeliveredOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.GeneralSettingsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.PlacedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ProfileFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ProcessedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.CancelledOrderFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;
    private final String category;



    public PagerAdapter(FragmentManager fm, int NumOfTabs,String category) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.category = category;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                if(category.equals("Setting"))
                {
                    return new ProfileFragment();
                }
                else if(category.equals("Order"))
                {
                    return new PlacedOrderFragment();
                }

                break;

            case 1:

                if(category.equals("Setting"))
                {
                    return new BillLayoutFragment();
                }
                else if(category.equals("Order"))
                {
                    return new ProcessedOrderFragment();
                }

                break;

            case 2:
                if(category.equals("Order"))
                {
                    return new DeliveredOrderFragment();

                }
                else if(category.equals("Setting"))
                {
                    return new GeneralSettingsFragment();
                }
                break;

            case 3:
                if(category.equals("Order"))
                {
                    return new CancelledOrderFragment();
                }
                break;

            default:
                return null;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}