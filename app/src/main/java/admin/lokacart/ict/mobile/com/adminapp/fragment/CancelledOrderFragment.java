package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderDetailsRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.adapter.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.container.SavedOrder;

/**
 * Created by Vishesh on 19-01-2016.
 */
public class CancelledOrderFragment extends Fragment  {

    private SwipeRefreshLayout swipeContainer;

    private ArrayList<SavedOrder> cancelledOrderArrayList;
    private View cancelledOrderFragmentView;
    private RecyclerView mRecyclerView;
    private int count =0;
    private TextView tOrders;
    public static final String TAG = "CancelledOrderFragment";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        cancelledOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);
        swipeContainer = (SwipeRefreshLayout) cancelledOrderFragmentView.findViewById(R.id.swipeRefreshLayout);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getActivity()))
                    new GetCancelledOrderDetails(false).execute();
                else
                   Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_bright);

        return cancelledOrderFragmentView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cancelledOrderArrayList = new ArrayList<>();
    }



    private ArrayList<SavedOrder> getOrders(JSONArray jsonArray) {

        ArrayList<SavedOrder> orders = new ArrayList<>();
        int size= jsonArray.length();


        if(size!=0)

        {

            int recyclerViewIndex;
            for(recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
            {

                try {

                    SavedOrder order = new SavedOrder((JSONObject) jsonArray.get(recyclerViewIndex), recyclerViewIndex, Master.CANCELLEDORDER);
                    orders.add(order);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }

        return orders;
    }





    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(Master.isNetworkAvailable(getActivity()))
            new GetCancelledOrderDetails(true).execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();


        mRecyclerView = (RecyclerView) cancelledOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        tOrders = (TextView) cancelledOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_cancelled_orders_present);
        tOrders.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

    }


    public void clickListener(int position)
    {

        RecyclerView mRecyclerView;
        final RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;

        Button processOrder,close,edit,delete;
        Space space,sProcessClose;
        TextView txtComments;

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.orders_details_list))
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)
                .setCancelable(true)
                .create();


        mRecyclerView = (RecyclerView)dialog.findViewById(R.id.orderdetailsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new OrderDetailsRecyclerViewAdapter(cancelledOrderArrayList.get(position).getItemsList(position));
        mRecyclerView.setAdapter(mAdapter);
        processOrder = (Button)dialog.findViewById(R.id.processOrderBtn);
        edit=(Button) dialog.findViewById(R.id.editOrderBtn);
        delete=(Button) dialog.findViewById(R.id.deleteOrderBtn);

        space=(Space)dialog.findViewById(R.id.txtspace);
        sProcessClose=(Space)dialog.findViewById(R.id.sProcessClose);
        txtComments=(TextView)dialog.findViewById(R.id.comments);
        processOrder.setVisibility(View.GONE);
        edit.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);


        space.setVisibility(View.GONE);
        sProcessClose.setVisibility(View.GONE);
        txtComments.setVisibility(View.VISIBLE);
        txtComments.setMovementMethod(new ScrollingMovementMethod());
        txtComments.setText(getString(R.string.textview_comments) + " " + cancelledOrderArrayList.get(position).getComment());

        close = (Button)dialog.findViewById(R.id.closeBtn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }
    public class GetCancelledOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        final Boolean showProgressDialog;

        public GetCancelledOrderDetails(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }


        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String cancelledOrderURL = Master.getCancelledOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(cancelledOrderURL, null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());

        }

        @Override
        protected void onPostExecute(String response) {
            if(showProgressDialog)
            {
                if(pd != null && pd.isShowing())
                    pd.dismiss();
            }


            if(CancelledOrderFragment.this.isAdded())
            {

                if (response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try
                    {

                        JSONObject responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("orders");
                        if(jsonArray.length() == 0)
                        {
                            mRecyclerView.setVisibility(View.GONE);
                            tOrders.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            tOrders.setVisibility(View.GONE);
                            ++count;

                            cancelledOrderArrayList = getOrders(jsonArray);

                            RecyclerView.Adapter mAdapter = new OrderRecyclerViewAdapter(cancelledOrderArrayList, getActivity(),TAG, false);
                            if(count < 2) {

                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.cancelledOrderKey, CancelledOrderFragment.this));
                                mAdapter.notifyDataSetChanged();
                            }
                            else{

                                mRecyclerView.swapAdapter(mAdapter,true);
                            }
                        }
                        swipeContainer.setRefreshing(false);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();

                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }
}

