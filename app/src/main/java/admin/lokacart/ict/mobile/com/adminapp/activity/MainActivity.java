package admin.lokacart.ict.mobile.com.adminapp.activity;

/**
 * Created by Vishesh on 29/12/15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import admin.lokacart.ict.mobile.com.adminapp.*;
import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivityAdminApp";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private GetOTPTask anp;
    private OTPCountDownTimer countDownTimer;
    private AddPasswordTask addPasswordTask;
    private Dialog dialog;
    private String number;
    private String pwd;
    private String pwd1;
    private String pwd2;
    private String otp_check;
    private long session;
    private EditText eMobileNumber;
    private EditText ePassword;
    private EditText eOTP;
    private EditText ePass1;
    private EditText ePass2;
    private Button bOTP;
    private Button bConfirm;
    private Button bCancel;
    private CheckBox chkBox;

    @Override
    protected void onResume() {
        super.onResume();
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        Master.getAdminData(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0d271a")));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#0d271a"));
        }
        if(Master.isNetworkAvailable(MainActivity.this))
        {
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            }
            catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String versionCode = String.valueOf(pInfo.versionCode);

            if(!sharedPreferences.getBoolean("logout", false)) {
                new CheckApkVersionTask(MainActivity.this).execute(versionCode);
            }
            else{
                editor.putBoolean("logout", false);
                editor.commit();
                initialize();
            }
        }
        else
        {
            if(sharedPreferences.getInt("versionchk", -1)==-1 || sharedPreferences.getInt("versionchk", -1) == 0)
                initialize();
            else if(sharedPreferences.getInt("versionchk", -1)==1)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(admin.lokacart.ict.mobile.com.adminapp.R.string.label_alertdialog_app_update_msg)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                initialize();
                            }
                        }).setCancelable(false);
                dialog = builder.show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.label_alertdialog_app_update_msg)
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        }).setCancelable(false);

                dialog = builder.show();
            }
        }
    }

    private void initialize()
    {
        Boolean login = sharedPreferences.getBoolean("login", false);

        if (login)
        {
            AdminDetails.setEmail(sharedPreferences.getString("emailid", "abc@def.com"));
            AdminDetails.setMobileNumber((sharedPreferences.getString("mobilenumber", "0000000000")));
            AdminDetails.setPassword(sharedPreferences.getString("password", "null"));
            AdminDetails.setAbbr(sharedPreferences.getString("abbr", "Abbr"));
            AdminDetails.setID(sharedPreferences.getString("org_id", "Org_ID"));
            AdminDetails.setName(sharedPreferences.getString("name", "Name"));

            startActivity(new Intent(MainActivity.this, DashboardActivity.class));
            finish();
        }
        else
        {
            setContentView(R.layout.activity_main);
            eMobileNumber = (EditText) findViewById(R.id.eMobileNumber);
            ePassword = (EditText) findViewById(R.id.ePassword);
            ePassword.setLongClickable(false);
            chkBox = (CheckBox) findViewById(R.id.chkBox);
            CheckBox showPasswordChkBox = (CheckBox) findViewById(R.id.showPasswordChkBox);

            if((!sharedPreferences.getString("mobilenumber","").equals("")&&!sharedPreferences.getString("password","").equals("")))
            {
                eMobileNumber.setText(sharedPreferences.getString("mobilenumber", ""));
                ePassword.setText(sharedPreferences.getString("password",""));
            }


            showPasswordChkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        ePassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    } else {
                        ePassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    }
                }
            });

            Button bLogin = (Button) findViewById(R.id.bLogin);
            bLogin.setOnClickListener(this);

            Button bForgotPassword = (Button) findViewById(R.id.bForgotPassword);
            bForgotPassword.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bLogin :
                login();
                break;

            case R.id.bForgotPassword :
                forgot();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    private void login()
    {
        number = eMobileNumber.getText().toString();
        pwd = ePassword.getText().toString();

        if(number.equals("") && pwd.equals(""))
        {
            Toast.makeText(MainActivity.this,R.string.label_toast_Please_enter_a_mobile_number_and_a_password, Toast.LENGTH_SHORT).show();
        }
        else if(number.equals(""))
        {
            Toast.makeText(MainActivity.this,R.string.label_toast_Please_enter_a_mobile_number, Toast.LENGTH_SHORT).show();
        }
        else if(pwd.equals(""))
        {
            Toast.makeText(MainActivity.this, R.string.label_toast_Please_enter_password, Toast.LENGTH_SHORT).show();
        }
        else if(number.length()!= 10)
        {
            Toast.makeText(getApplicationContext(), R.string.label_toast_enter_valid_number, Toast.LENGTH_SHORT).show();
        }
        else if(Master.isNetworkAvailable(MainActivity.this))
        {
            JSONObject obj = new JSONObject();
            try
            {
                obj.put("phonenumber","91"+number);
                obj.put("password",pwd);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


            LoginVerification loginVerification = new LoginVerification(MainActivity.this);
            loginVerification.execute(obj);
        }
        else
        {
            Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }





    private void forgot()   {

        number = eMobileNumber.getText().toString();

        if(!number.equals(""))
        {
            if(Master.isNetworkAvailable(MainActivity.this))
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_label_member_verification);
                builder.setPositiveButton(R.string.builder_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                builder.setNegativeButton(R.string.label_button_getOTP, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("phonenumber", "91" + number);
                            anp = new GetOTPTask(MainActivity.this);
                            anp.execute(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.create();
                builder.show();
                //this dialog box is shown when getOTP task is called
                dialog=new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.new_password_box);
                dialog.setTitle(R.string.dialog_label_member_verification);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                eOTP = (EditText) dialog.findViewById(R.id.eOTP);
                eOTP.setVisibility(View.GONE);

                ePass1 = (EditText) dialog.findViewById(R.id.ePass1);
                ePass1.setVisibility(View.GONE);

                ePass2 = (EditText) dialog.findViewById(R.id.ePass2);
                ePass2.setVisibility(View.GONE);

                bConfirm = (Button) dialog.findViewById(R.id.bConfirm);
                bConfirm.setVisibility(View.GONE);

                bOTP = (Button) dialog.findViewById(R.id.bOTP);
                bOTP.setVisibility(View.GONE);

                bCancel = (Button) dialog.findViewById(R.id.bCancel);
                bCancel.setVisibility(View.GONE);


                bOTP.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {

                                                   JSONObject obj = new JSONObject();
                                                   try {
                                                       obj.put("phonenumber", "91" + number);
                                                       anp = new GetOTPTask(MainActivity.this);
                                                       anp.execute(obj);
                                                   } catch (JSONException e) {
                                                       e.printStackTrace();
                                                   }
                                               }
                                           }
                );


                bCancel.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View v)
                                               {

                                                   if(countDownTimer!=null)countDownTimer.cancel();

                                                   dialog.dismiss();
                                               }
                                           }
                );

                bConfirm.setOnClickListener(new View.OnClickListener()
                                            {
                                                @Override
                                                public void onClick(View v)
                                                {

                                                    long session2 = System.currentTimeMillis();
                                                    if(session2<session)
                                                    {
                                                        if(eOTP.getText().toString().equals(otp_check))
                                                        {
                                                            countDownTimer.cancel();
                                                            pwd1 = ePass1.getText().toString();
                                                            pwd2 = ePass2.getText().toString();

                                                            if(!pwd1.equals(""))
                                                            {
                                                                if(pwd1.equals(pwd2)){
                                                                    try
                                                                    {
                                                                        JSONObject addPasswordDB= new JSONObject();
                                                                        addPasswordDB.put("phonenumber","91"+number);
                                                                        addPasswordDB.put("password", pwd1);
                                                                        if(Master.isNetworkAvailable(MainActivity.this))
                                                                        {
                                                                            addPasswordTask = new AddPasswordTask(MainActivity.this);
                                                                            addPasswordTask.execute(addPasswordDB);
                                                                        }
                                                                        else
                                                                        {
                                                                            Toast.makeText(getApplicationContext(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                    catch(JSONException e)
                                                                    {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    ePass1.setText("");
                                                                    ePass2.setText("");
                                                                    Toast.makeText(getApplicationContext(),R.string.label_toast_Passwords_do_not_match, Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ePass1.setText("");
                                                                ePass2.setText("");
                                                                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_enter_password, Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            eOTP.setHint(R.string.hint_enter_otp_received);
                                                            Toast.makeText(getApplicationContext(), R.string.label_toast_Please_enter_correct_OTP, Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    else
                                                    {
                                                        eOTP.setText("");
                                                        eOTP.setHint(R.string.hint_enter_otp_received);
                                                        eOTP.setEnabled(false);

                                                        ePass1.setText("");
                                                        ePass1.setHint(R.string.hint_enter_new_password);
                                                        ePass1.setEnabled(false);

                                                        ePass2.setText("");
                                                        ePass2.setHint(R.string.hint_reenter_new_password);
                                                        ePass2.setEnabled(false);

                                                        bOTP.setEnabled(true);
                                                        bConfirm.setVisibility(View.GONE);
                                                        bConfirm.setEnabled(false);
                                                        bCancel.setVisibility(View.VISIBLE);
                                                        bOTP.setVisibility(View.VISIBLE);

                                                        Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_expired, Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                );

            }
             else
            {
                Toast.makeText(this,R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this,R.string.label_toast_Please_enter_mobile_number,Toast.LENGTH_SHORT).show();
        }
    }


    public class GetOTPTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        final Context context;

        public GetOTPTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url = Master.getForgotPasswordURL();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(url,params[0],"POST",false,null,null);
        }

        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();

            if(response.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_we_are_facing_some_technical_problems_main_page), getString(R.string.label_alertdialog_ok));
            }
            else
            {

                try {

                    JSONObject obj = new JSONObject(response);

                    otp_check = obj.getString("otp");

                    if(otp_check.equals("null")){
                        Toast.makeText(getApplicationContext(), R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_sent_to_email,Toast.LENGTH_SHORT).show();

                        dialog.setTitle(R.string.dialog_title_set_new_password);
                        eOTP.setVisibility(View.VISIBLE);
                        eOTP.setEnabled(true);
                        ePass1.setVisibility(View.VISIBLE);
                        ePass2.setVisibility(View.VISIBLE);
                        ePass1.setEnabled(true);
                        ePass2.setEnabled(true);
                        bOTP.setVisibility(View.GONE);
                        bConfirm.setVisibility(View.VISIBLE);
                        bConfirm.setEnabled(true);
                        bCancel.setVisibility(View.VISIBLE);

                        dialog.show();

                        session = System.currentTimeMillis();
                        countDownTimer = new OTPCountDownTimer();
                        countDownTimer.start();
                        session = session + 600000;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public class  AddPasswordTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        final Context context;

        public AddPasswordTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url = Master.getChangePasswordURL();

            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(url,params[0],"POST",false,null,null);
        }


        @Override
        protected void onPostExecute(String response) {
            if(pd != null && pd.isShowing())
                pd.dismiss();


            if(response.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_we_are_facing_some_technical_problems_main_page), getString(R.string.label_alertdialog_ok));
            }
            else
            {
                try {

                    JSONObject obj = new JSONObject(response);
                    String msg = obj.getString("Status");

                    if(msg.equals("Success")) {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Password_successfully_changed, Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    dialog.dismiss();
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
    }



    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer()
        {
            super((long) 600000, (long) 1000);
        }

        @Override
        public void onFinish()
        {
            eOTP.setText("");
            eOTP.setHint(R.string.hint_enter_otp_received);
            eOTP.setEnabled(false);
            bCancel.setEnabled(true);
            bCancel.setVisibility(View.VISIBLE);
            bOTP.setEnabled(true);
            bOTP.setVisibility(View.VISIBLE);

            ePass1.setText("");
            ePass1.setHint(R.string.hint_enter_new_password);
            ePass1.setEnabled(true);

            ePass2.setText("");
            ePass2.setHint(R.string.hint_reenter_new_password);
            ePass2.setEnabled(true);

            ePass1.setVisibility(View.VISIBLE);
            ePass2.setVisibility(View.VISIBLE);

            bConfirm.setEnabled(false);
            bConfirm.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_expired, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }

    public class LoginVerification extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        final Context context;

        public LoginVerification(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = GetJSON.getInstance();
            return getJson.getJSONFromUrl(Master.getLoginURL(),params[0],"POST",false,null,null);
        }

        @Override
        protected void onPostExecute(String response) {
            if(pd != null && pd.isShowing())
                pd.dismiss();
            if (response.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_we_are_facing_some_technical_problems_main_page), getString(R.string.label_alertdialog_ok));
            }
            else
            {
                try
                {

                    JSONObject resObj = new JSONObject(response);
                    if (resObj.get("Authentication").toString().equals("success")) {
                        AdminDetails.setMobileNumber(number);
                        AdminDetails.setPassword(pwd);

                        try {

                            String emailId = resObj.getString("email");
                            AdminDetails.setEmail(emailId);

                            JSONObject jsonObject = resObj.getJSONObject("organization");

                            AdminDetails.setAbbr(jsonObject.getString("abbr"));
                            AdminDetails.setID(jsonObject.getString("org_id"));
                            AdminDetails.setName(jsonObject.getString("name"));

                            editor.putBoolean("login", true);
                            editor.putBoolean("logout", false);
                            if(chkBox.isChecked())
                            {
                                editor.putString("signchk", "true");
                                editor.putString("emailid",emailId);
                                editor.putString("password",pwd);
                                editor.putString("mobilenumber",number);
                                editor.putString("name", AdminDetails.getName());
                                editor.putString("abbr", AdminDetails.getAbbr());
                                editor.putString("org_id", AdminDetails.getID());
                            }
                            else
                            {
                                editor.putString("signchk", "false");
                                editor.putString("mobilenumber","");
                                editor.putString("password","");
                            }
                            editor.commit();

                            Intent i = new Intent(MainActivity.this, DashboardActivity.class);
                            startActivityForResult(i, 0);
                            finish();

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {

                        if(resObj.get("registered").toString().equals("false"))
                        {
                            Toast.makeText(getApplicationContext(),R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_SHORT).show();
                            eMobileNumber.setText("");
                            ePassword.setText("");
                        }
                        else
                        {
                            String error = resObj.get("Error").toString();
                            if(error.equals("null"))
                            {
                                Toast.makeText(getApplicationContext(), R.string.label_mobile_number_password_mismatch, Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                }


            }

        }
    }


    public class CheckApkVersionTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        final Context context;

        public CheckApkVersionTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.pd_checking_for_updates));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getCheckApkURL(params[0]), null, "GET", true, null, null);
        }

        @Override
        protected void onPostExecute(String response) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            if (response.equals("exception"))
            {
                editor.putInt("versionchk",-1);
                editor.commit();
                initialize();
            }
            else
            {
                try {
                    JSONObject obj = new JSONObject(response);
                    String msg = obj.getString("response");
                    if(msg.equals("0"))
                    {
                        editor.putInt("versionchk", 0);
                        editor.commit();
                        initialize();
                    }

                    if(msg.equals("1"))
                    {
                        editor.putInt("versionchk", 1);
                        editor.commit();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        initialize();
                                    }
                                }).setCancelable(false);
                        dialog = builder.show();
                    } else {

                        if(msg.equals("2")) {

                            editor.putInt("versionchk",2);
                            editor.commit();

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                            builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                    .setPositiveButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    }).setCancelable(false);
                            dialog = builder.show();
                        }
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
    }


}
