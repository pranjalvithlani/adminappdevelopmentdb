package admin.lokacart.ict.mobile.com.adminapp.interfaces;

/**
 * Created by Vishesh on 08-01-2016.
 */
public interface MyListener {
     void onCardClickListener(int position,int cat, Object obj);
     void onCardLongClickListener(int position, int category);
}
