package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Date;


import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.adapter.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.adapter.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.container.SavedOrder;

/**
 * Created by Vishesh on 19-01-2016.
 */
public class ProcessedOrderFragment extends Fragment  {

    private SwipeRefreshLayout swipeContainer;
    private static ArrayList<SavedOrder> processedOrderArrayList;
    private View processedOrderFragmentView;
    private RecyclerView mRecyclerView;
    private static RecyclerView.Adapter mAdapter;
    private JSONObject responseObject;
    private static int recyclerViewIndex;
    private int count=0;
    private TextView  tOrders;

    public static final String TAG = "ProcessedOrderFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        processedOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        return processedOrderFragmentView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        processedOrderArrayList = new ArrayList<>();
    }




    private ArrayList<SavedOrder> getOrders(JSONArray jsonArray) {

        ArrayList<SavedOrder> orders = new ArrayList<>();
        int size= jsonArray.length();


        if(size!=0)

        {

            for(recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
            {

                try {

                    SavedOrder order = new SavedOrder((JSONObject) jsonArray.get(recyclerViewIndex), recyclerViewIndex, Master.PROCESSEDORDER);
                    orders.add(order);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }

        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
            new GetProcessedOrderDetails(true).execute();
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

        mRecyclerView = (RecyclerView) processedOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        swipeContainer = (SwipeRefreshLayout) processedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getActivity()))
                    new GetProcessedOrderDetails(false).execute();
                else
                   Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);


        tOrders = (TextView) processedOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_processed_orders_present);
        tOrders.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }




    public void clickListener(int position)
    {
        if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
            new ViewBillAsyncTask(getActivity(),position).execute("" + processedOrderArrayList.get(position).getOrderId());
        else
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
    }

    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        final Context context;
        ProgressDialog pd;
        final int position;

        ViewBillAsyncTask(Context context,int position) {
            this.context = context;
            this.position=position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {


                URL linkurl = new URL(Master.getProcessedOrderViewBillURL(AdminDetails.getAbbr(),params[0]));
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is;
                status=String.valueOf(linkConnection.getResponseCode());
                if(status.equals("200"))
                {
                    is=linkConnection.getInputStream();
                }
                else
                {
                    status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(pd != null && pd.isShowing())
            pd.dismiss();

            if(ProcessedOrderFragment.this.isAdded()){
                switch (s) {
                    case "exception":
                        Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                        break;
                    case "failure":
                        Toast.makeText(getActivity(), R.string.label_toast_order_already_processed, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setTitle(R.string.dialog_title_bill);
                        WebView wv = new WebView(context);
                        wv.loadData(s, "text/html", "UTF-8");
                        wv.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                        alert.setView(wv);

                        alert.setNegativeButton(R.string.label_alertdialog_delivered, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder confirm_alert = new AlertDialog.Builder(context);

                                confirm_alert.setTitle(R.string.title_alertdialog_are_you_sure_order_is_delivered);

                                confirm_alert.setPositiveButton(getString(R.string.label_alertdialog_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("status", "delivered");
                                            //For sending the current date to match it with the one in database to confirm which is the real delivery date

                                            Date date = new Date();
                                            Timestamp ts = new Timestamp(date.getTime());
                                            jsonObject.put("deldate",String.valueOf(ts));

                                        } catch (JSONException ignored)
                                        {
                                        }
                                        new ChangeToDeliveredOrderTask(processedOrderArrayList.get(position).getOrderId(), position).execute(jsonObject);

                                    }
                                });
                                confirm_alert.setNegativeButton(getString(R.string.label_button_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                confirm_alert.show();

                            }
                        });
                        alert.setPositiveButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                        break;
                }
            }

        }
    }



    public class GetProcessedOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        final Boolean showProgressDialog;

        public GetProcessedOrderDetails(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String processedOrderURL = Master.getProcessedOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(processedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog)
                if(pd != null && pd.isShowing())
                    pd.dismiss();

            if (response.equals("exception")) {
                Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
            } else {
                try {
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("orders");
                    if (jsonArray.length() == 0) {
                        tOrders.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tOrders.setVisibility(View.GONE);
                        ++count;

                        processedOrderArrayList = getOrders(jsonArray);

                        mAdapter = new OrderRecyclerViewAdapter(processedOrderArrayList, getActivity(),TAG, false);
                        if (count < 2) {

                            mRecyclerView.setAdapter(mAdapter);
                            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.processedOrderKey, ProcessedOrderFragment.this));
                            mAdapter.notifyDataSetChanged();
                        } else {

                            mRecyclerView.swapAdapter(mAdapter, true);
                        }
                    }

                    swipeContainer.setRefreshing(false);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                }
            }
        }

    }

    public class ChangeToDeliveredOrderTask extends AsyncTask<JSONObject,String,String>{
        ProgressDialog pd;
        final Boolean showProgressDialog;
        final int orderID;
        final int position;

        public ChangeToDeliveredOrderTask(int orderID, int position) {
            this.showProgressDialog = true;
            this.orderID=orderID;
            this.position=position;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getChangeDeliveredState(orderID), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog)
                if (pd != null && pd.isShowing())
                    pd.dismiss();

            if(ProcessedOrderFragment.this.isAdded())
            {

                if(response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems),getString(R.string.label_alertdialog_ok));

                }
                else
                {
                    try {
                        responseObject = new JSONObject(response);
                        response = responseObject.getString("status");
                        switch (response)
                        {

                            case "Success":
                                Toast.makeText(getActivity(), R.string.label_toast_status_changed_to_delivered, Toast.LENGTH_SHORT).show();
                                processedOrderArrayList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                recyclerViewIndex--;
                                if (recyclerViewIndex == 0) {
                                    tOrders.setVisibility(View.VISIBLE);
                                    mRecyclerView.setVisibility(View.GONE);
                                } else {
                                    tOrders.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                }

                                break;
                            case "error":
                                try {
                                    response = responseObject.getString("error");
                                } catch (JSONException ignored) {

                                }
                                response = response+" "+getString(R.string.label_please_refresh);
                                Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();

                                break;
                            default:

                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }



                }


            }

            }



        }


    }



