package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.adapter.PagerAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;


/**
 * Created by Vishesh on 18-01-2016.
 */
public class SettingsFragment extends Fragment {

    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        if(SettingsFragment.this.isAdded() && getActivity()!=null)
        ((DashboardActivity)getActivity()).updateStatusBarColor();

        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
        getActivity().setTitle(R.string.title_settings);
        setRetainInstance(true);
        final TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_profile_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_billlayout_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_general_settings)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        FragmentManager fm = getFragmentManager();
        PagerAdapter mPageAdapter = new PagerAdapter(fm, tabLayout.getTabCount(), "Setting");
        viewPager = (ViewPager)rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                DashboardActivity.resetBackPress();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
