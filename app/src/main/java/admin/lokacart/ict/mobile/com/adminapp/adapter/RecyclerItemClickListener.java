package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.MyListener;

/**
 * Created by Vishesh on 13-01-2016.
 */
public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener{
    private final MyListener callback;
    private final int cat;
    private final Object obj;

    private final GestureDetector mGestureDetector;

    public RecyclerItemClickListener(Context context,final RecyclerView recyclerView, final int cat, final Object obj)
    {
        callback = (MyListener) context;
        this.cat = cat;
        this.obj= obj;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
            @Override
            public void onLongPress(MotionEvent e)
            {
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if(childView != null)
                {
                    DashboardActivity.resetBackPress();
                    callback.onCardLongClickListener(recyclerView.getChildAdapterPosition(childView), cat);
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e)
    {
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if(childView != null && mGestureDetector.onTouchEvent(e))
        {
            DashboardActivity.resetBackPress();
            callback.onCardClickListener(recyclerView.getChildAdapterPosition(childView), cat, obj);
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }


}
