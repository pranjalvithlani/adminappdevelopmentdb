package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.util.Validation;

/**
 * Created by root on 3/2/16.
 */
public class ProfileFragment  extends Fragment {

    private EditText userName;
    private EditText emailId;
    private EditText password;
    private EditText confirmPassword;
    private EditText phoneNumber;
    private EditText city;
    private EditText eOtp;
    private String name;
    private String email;
    private String cit;
    private String localMobileNumber;
    private Button bPositive;
    private Button bNeutral;
    private AlertDialog changeMobileNumberAlert;
    private View rootView;
    private View editMobileNumberDialogView;
    private long session;
    private JSONObject responseObject;
    private OTPCountDownTimer countDownTimer;
    private String otp_check;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.profile, container, false);
        getActivity().setTitle(R.string.title_settings);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        userName = (EditText)rootView.findViewById(R.id.username);
        emailId = (EditText)rootView.findViewById(R.id.email);
        password = (EditText)rootView.findViewById(R.id.password);
        confirmPassword = (EditText)rootView.findViewById(R.id.confirmpassword);
        phoneNumber = (EditText)rootView.findViewById(R.id.contact);
        city = (EditText)rootView.findViewById(R.id.city);
        Button save = (Button) rootView.findViewById(R.id.save);
        localMobileNumber=AdminDetails.getMobileNumber();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null && Master.isNetworkAvailable(getActivity())) {
                    if (name.equals(userName.getText().toString().trim())
                            && email.equals(emailId.getText().toString().trim())
                            && cit.equals(city.getText().toString().trim())
                            && password.getText().toString().trim().equals("")
                            && confirmPassword.getText().toString().trim().equals(""))
                        Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                    else if (checkValidation())
                        updateProfile();
                    else
                        Toast.makeText(getActivity(), R.string.label_toast_Form_contains_error, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }

        });

        ImageView ivMobileNumberDone = (ImageView) rootView.findViewById(R.id.ivMobileNumberDone);
        ivMobileNumberDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneNumber.getText().toString().trim().length() != 10)
                    Toast.makeText(getActivity(), R.string.label_toast_enter_valid_number, Toast.LENGTH_LONG).show();
                else if (phoneNumber.getText().toString().trim().charAt(0) == '0')
                    Toast.makeText(getActivity(), R.string.label_toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
                else if (localMobileNumber.equals(phoneNumber.getText().toString().trim()))
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_LONG).show();
                else {
                    if(Master.isNetworkAvailable(getActivity()))
                    {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("phonenumber_old", "91" + AdminDetails.getMobileNumber());
                            obj.put("phonenumber_new", "91" + phoneNumber.getText().toString().trim());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //call the Api to verify Mobile number

                        new MobileNumberVerifyTask(2).execute(obj);
                    }
                    else
                    {
                        Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                    }

                }
            }
        });

        if(Master.isNetworkAvailable(getActivity()))
            new GetProfileTask(getActivity()).execute();
        else
            Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

    }

    private void updateProfile() {
        JSONObject profileObj = new JSONObject();
        try
        {
            profileObj.put("name", userName.getText().toString().trim());
            profileObj.put("email", emailId.getText().toString().trim());
            profileObj.put("address", city.getText().toString().trim());
            profileObj.put("phonenumber", "91"+ AdminDetails.getMobileNumber().trim());
            if(!password.getText().toString().trim().equals("") && confirmPassword.getText().toString().trim().equals(""))
            {
                Validation.hasText(getActivity(), confirmPassword);
            }
            else if(password.getText().toString().trim().equals("")&&!confirmPassword.getText().toString().trim().equals(""))
            {
                Validation.hasText(getActivity(), password);
            }
            else if(!password.getText().toString().trim().equals("")&&!confirmPassword.getText().toString().trim().equals(""))
            {
                if(Validation.isPasswordMatch(getActivity(), confirmPassword, password.getText().toString().trim()))
                {
                    profileObj.put("password", password.getText().toString().trim());
                    if(Master.isNetworkAvailable(getActivity()))
                        new UpdateProfileTask(getActivity()).execute(profileObj);
                    else
                        Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
            else if(Master.isNetworkAvailable(getActivity()))
                new UpdateProfileTask(getActivity()).execute(profileObj);
            else
                Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

//-----------------------------get profile task---------------------------------------------------------------

    public class  GetProfileTask extends AsyncTask<String,String, String> {

        ProgressDialog pd;
        final Context context;
        public GetProfileTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String profileURL = Master.serverURL + "api/" + AdminDetails.getAbbr()+"/profilesettingsview?number=91"+AdminDetails.getMobileNumber();
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(profileURL,null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(ProfileFragment.this.isAdded()){
                if(response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try {
                        JSONObject resObj = new JSONObject(response);
                        if(resObj.getString("response").equals("success")) {
                            userName.setText(resObj.getString("name"));
                            city.setText(resObj.getString("address"));
                            String phnumber = resObj.getString("phonenumber");
                            phnumber = phnumber.substring(2);
                            phoneNumber.setText(phnumber);
                            emailId.setText(resObj.getString("email"));

                            name = resObj.getString("name");
                            cit = resObj.getString("address");
                            email = resObj.getString("email");
                        }
                        else {
                            Toast.makeText(getActivity(), resObj.getString("Error")+R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }
//----------------------------------------------------------------------------------------------------------


//-----------------------------------update profile---------------------------------------------------

    public class  UpdateProfileTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        final Context context;
        JSONObject jsonObject;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;

        public UpdateProfileTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            jsonObject = params[0];
            String profileURL = Master.serverURL + "api/" + AdminDetails.getAbbr()+"/profilesettingsupdate";
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(profileURL, params[0], "POST",true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(ProfileFragment.this.isAdded()){
                if(response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try
                    {
                        JSONObject resObj = new JSONObject(response);
                        if(resObj.getString("response").equals("Successfully updated"))
                        {
                            Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_Your_profile_updated_successfully), getString(R.string.label_alertdialog_ok));
                            ArrayList<TextView> textViews = DashboardActivity.getDrawerTextViews();
                            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            editor = sharedPreferences.edit();
                            if(AdminDetails.getEmail() != jsonObject.getString("email"))
                            {
                                AdminDetails.setEmail(jsonObject.getString("email"));
                                textViews.get(1).setText(AdminDetails.getEmail());
                                editor.putString("emailid", AdminDetails.getEmail());
                            }

                            if(jsonObject.has("password") && AdminDetails.getPassword() != jsonObject.get("password"))
                            {
                                AdminDetails.setPassword(jsonObject.getString("password"));
                                editor.putString("password", AdminDetails.getPassword());
                            }
                            editor.commit();

                            email=emailId.getText().toString();
                            name=userName.getText().toString();
                            cit=city.getText().toString();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), resObj.getString("Error")+R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                            if(resObj.getString("Error").equals("Email Exists"))
                                emailId.setError(getString(R.string.label_email_already_exists));

                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }
    }
//------------------------------------------------------------------------------------------------------
    private boolean checkValidation()
    {
        boolean ret = true;
        if (!Validation.hasText(getActivity(), userName)) ret = false;
        if (!Validation.isEmailAddress(getActivity(), emailId)) ret = false;
        if (!Validation.hasText(getActivity(), city)) ret = false;
        return ret;
    }

   //--------------------------------------------------------------------------------------------------//

    public class MobileNumberVerifyTask extends AsyncTask<JSONObject,String,String>{
        ProgressDialog pd;
        final int category;

        public MobileNumberVerifyTask(int cat){
            category=cat;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getNumberVerifyURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(ProfileFragment.this.isAdded()){

                if(response.equals("exception")){
                    AlertDialog.Builder alertDialog=new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle(R.string.label_we_are_facing_some_technical_problems);
                    alertDialog.setNegativeButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                }else{

                    try {
                        responseObject = new JSONObject(response);
                        String otp_val =  responseObject.getString("otp");
                        String text =  responseObject.getString("text");
                        if(!otp_val.equals("null")&&!text.equals("null")) {
                            Toast.makeText(getActivity(),R.string.label_toast_otp_sent, Toast.LENGTH_LONG).show();

                            otp_check = otp_val;
                            session = System.currentTimeMillis();
                            countDownTimer = new OTPCountDownTimer();
                            countDownTimer.start();

                            session = session + 600000; // 10 minutes

                            if(category == 2)
                                editMobileNumber();
                            else
                            {
                                bNeutral.setVisibility(View.GONE);
                                bPositive.setVisibility(View.VISIBLE);
                            }


                        }else{
                            if(otp_val.equals("null")&&text.equals("Phone number entered already exists."))
                            {
                                Toast.makeText(getActivity(),R.string.label_toast_phone_number_already_exists, Toast.LENGTH_LONG).show();
                            }
                        }

                        }
                    catch(JSONException e){
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                    }

                }

            }

        }
    }
    //---------------------------------------OTP Count Down Timer-------------------------------------------------
    public class OTPCountDownTimer extends CountDownTimer
    {
        public OTPCountDownTimer()
        {
            super((long) 600000, (long) 1000);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            if(ProfileFragment.this.isAdded()){
                if(editMobileNumberDialogView != null)
                {
                    eOtp.setText("");
                    bPositive.setVisibility(View.GONE);
                    bNeutral.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),R.string.label_toast_OTP_expired, Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    //----------------------------------editMobileNumberfunction-------------------------------------


    private void editMobileNumber(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        editMobileNumberDialogView = getActivity().getLayoutInflater().inflate(R.layout.change_mobile_number, null);
        builder.setView(editMobileNumberDialogView);
        builder.setTitle(R.string.builder_title_change_mobile_number);
        builder.setPositiveButton(R.string.builder_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNeutralButton(R.string.label_button_getOTP, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton(R.string.builder_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        eOtp = (EditText) editMobileNumberDialogView.findViewById(R.id.eOtp1);
        changeMobileNumberAlert = builder.create();
        changeMobileNumberAlert.show();

        bPositive = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_POSITIVE);
        Button bNegative = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_NEGATIVE);
        bNeutral = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_NEUTRAL);
        bNeutral.setVisibility(View.GONE);

        bNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMobileNumberAlert.dismiss();
            }
        });

        bNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                {

                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("phonenumber_old","91"+ AdminDetails.getMobileNumber());
                        obj.put("phonenumber_new","91" + phoneNumber.getText().toString().trim());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new MobileNumberVerifyTask(1).execute(obj);
                }
                else
                {
                    Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                }
            }
        });

        bPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long session2 = System.currentTimeMillis();

                if(session2<session)
                {
                    if(eOtp.getText().toString().trim().isEmpty())
                    {
                        Toast.makeText(getActivity(),R.string.textview_enter_otp_received, Toast.LENGTH_LONG).show();
                    }
                    else if(eOtp.getText().toString().trim().equals(otp_check))
                    {
                        changeMobileNumberAlert.dismiss();
                        countDownTimer.cancel();

                        if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                        {
                            JSONObject jsonObject = new JSONObject();
                            try
                            {
                                jsonObject.put("phonenumber", "91" + AdminDetails.getMobileNumber());
                                jsonObject.put("phonenumber_new", "91"+ phoneNumber.getText().toString().trim());


                                new EditMobileNumberTask(getActivity(), AdminDetails.getMobileNumber(),
                                        phoneNumber.getText().toString().trim()).execute(jsonObject);
                            }
                            catch (JSONException e)
                            {
                                Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                            }
                          /*  catch(Exception e){
                                e.printStackTrace();
                                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                            }  */
                        }
                        else
                        {
                            Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(),R.string.label_toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    eOtp.setText("");
                    Toast.makeText(getActivity(),R.string.label_toast_OTP_expired, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    //--------------------------------------EditMobileNumberTask-------------------------------------

    public class EditMobileNumberTask extends AsyncTask<JSONObject,String,String>
    {
        ProgressDialog pd;
        final Context context;
        final String oldNum;
        final String newNum;

        public EditMobileNumberTask(Context context, String oldNum, String newNum) {
            this.context = context;
            this.newNum = newNum;
            this.oldNum = oldNum;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getChangeNumberURL(), params[0],"POST",true,AdminDetails.getEmail(),AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd!=null && pd.isShowing()){
                pd.dismiss();
            }

            if(ProfileFragment.this.isAdded()){
                if(response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems),getString(R.string.label_alertdialog_ok));

                }
                else
                {
                    try {
                        responseObject = new JSONObject(response);

                        if(responseObject.getString("status").equals("success")){
                            Toast.makeText(getActivity(),R.string.label_toast_contact_updated_succesfully,Toast.LENGTH_LONG).show();
                            phoneNumber.setText(newNum);
                            localMobileNumber=newNum;
                            AdminDetails.setMobileNumber(newNum);

                        }

                    }catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        }
    }

}