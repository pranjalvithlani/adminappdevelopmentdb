package admin.lokacart.ict.mobile.com.adminapp.activity;

/**
 * Created by Vishesh on 29/12/15.
 */

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.LokacartAdminApplication;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.interfaces.NavigationItemListener;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.gcm.RegistrationIntentService;
import admin.lokacart.ict.mobile.com.adminapp.fragment.AboutUsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.BroadcastMessage;
import admin.lokacart.ict.mobile.com.adminapp.fragment.CancelledOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ContactUsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.DashboardFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.DeliveredOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ExistingUserFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.FAQFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.OrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.PlacedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ProcessedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.ProductTypeFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.SettingsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragment.TermsAndConditionsFragment;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,NavigationItemListener, MyListener {

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SharedPreferences.Editor editor;
    private FragmentManager fragmentManager;
    private final Bundle args = new Bundle();
    private static TextView tDashboardMobileNumber;
    private static TextView tDashboardName;
    private static TextView tDashboardEmail;
    private static boolean dashboardUpdate;
    public static boolean onBackDashboard;
    private String dashboard_TAG;
    private final static String TAG = "DashboardActivity";
    public static final String MEMBER_TAG = "Member";
    private static final String PRODUCT_TAG = "Product";
    private final static int REQUEST_PERMISSIONS =20;
    private NavigationView navigationView;
    public static FloatingActionButton fab;
    private long mBackPressed=0;
    private static final int TIME_INTERVAL = 2000;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static void resetBackPress() {
        Master.backPress = 1;
    }

    //adapter to set the list view in search functionality
    private static ArrayAdapter<String> itemsAdapter;

    //list to store products based on entered query
    private static ArrayList<String> newProducts;

    //list of products and their positions
    private static ArrayList<String> products;

    //declare a variable to get query in search field
    private String query_entered=null;

    private ListView listView;

    private String productDescription;
    private String productName;
    private String productId;
    private double productUnitPrice;
    private int stockQuantity;

    private int productTypePos;
    private int productPos;

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,new IntentFilter(Master.ACTION));
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);

        Master.getAdminData(getApplicationContext());

    }

    public Context getContext() {
        return DashboardActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Master.getAdminData(getApplicationContext());
        onBackDashboard = false;
        dashboard_TAG = "DASHBOARD_TAG";
        dashboardUpdate = false;
        super.onCreate(savedInstanceState);
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);
        setContentView(R.layout.activity_dashboard);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        if (checkPlayServices()) {
            startService(intent);
        }


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Master.ACTION))
                {
                    try {

                        int params[] = new int[9];

                        params[0] = Integer.parseInt(intent.getStringExtra("saved"));
                        params[1] = Integer.parseInt(intent.getStringExtra("processed"));
                        params[2] = Integer.parseInt(intent.getStringExtra("cancelled"));
                        params[3] = Integer.parseInt(intent.getStringExtra("totalUsers"));
                        params[4] = Integer.parseInt(intent.getStringExtra("newUsersToday"));
                        params[5] = Integer.parseInt(intent.getStringExtra("pendingUsers"));
                        params[6] = Integer.parseInt(intent.getStringExtra("paid"));
                        params[7] = Integer.parseInt(intent.getStringExtra("unpaid"));
                        params[8] = Integer.parseInt(intent.getStringExtra("delivered"));

                        DashboardFragment dashboardFragment = (DashboardFragment) getSupportFragmentManager().findFragmentByTag(dashboard_TAG);
                        if (dashboardFragment != null && dashboardFragment.isVisible()) {
                            dashboardFragment.update(params);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                       // Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();

        Context context = getApplicationContext();
        Activity activity = DashboardActivity.this;
        CheckPermissions.reqPerm(context, activity);

        tDashboardEmail = (TextView) findViewById(R.id.tDashboardEmail);
        tDashboardEmail.setText(AdminDetails.getEmail());

        tDashboardName = (TextView)findViewById(R.id.tDashboardName);
        tDashboardName.setText(AdminDetails.getName());

        tDashboardMobileNumber = (TextView)findViewById(R.id.tDashboardMobileNumber);
        tDashboardMobileNumber.setText(AdminDetails.getMobileNumber());

        fab = (FloatingActionButton) findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExistingUserFragment existingUserFragment = (ExistingUserFragment) getSupportFragmentManager().findFragmentByTag(MEMBER_TAG);
                if (existingUserFragment != null && existingUserFragment.isVisible()) {
                    existingUserFragment.addNewMember();
                } else {
                    ProductTypeFragment productTypeFragment = (ProductTypeFragment) getSupportFragmentManager().findFragmentByTag(PRODUCT_TAG);
                    if (Master.fabClickKey == 0)
                        productTypeFragment.addNewProductType();
                    else
                        productTypeFragment.saveChanges();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
    }


    public static ArrayList<TextView> getDrawerTextViews() {
        ArrayList<TextView> textViews = new ArrayList<>();
        textViews.add(0, tDashboardName);
        textViews.add(1, tDashboardEmail);
        textViews.add(2, tDashboardMobileNumber);
        return textViews;
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ProductTypeFragment myFragment = (ProductTypeFragment) getSupportFragmentManager().findFragmentByTag(PRODUCT_TAG);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (myFragment != null && myFragment.isVisible() && Master.backCheck == 1)
        {
            fab.setVisibility(View.VISIBLE);
            navigationView.getMenu().getItem(1).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductTypeFragment(), PRODUCT_TAG).commit();
            Master.backCheck = 0;

        } else if (onBackDashboard) {
            onBackDashboard = false;
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
        } else if (Master.backPress==2) {

            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
            {

                finish();
            }else {
                Master.backPress=1;
                onBackPressed();
            }

        } else if(Master.backPress==1){

            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            Snackbar snackbar = Snackbar.make(coordinatorLayout, R.string.label_toast_press_back_once_more_to_exit, Snackbar.LENGTH_SHORT);
            snackbar.show();
            mBackPressed = System.currentTimeMillis();
            Master.backPress = 2;

        }
        else{

            Master.backPress=1;

        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        resetBackPress();
        onBackDashboard = false;

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(navigationView.getWindowToken(),0);

        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(0).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
            dashboardUpdate = false;

        } else if (id == R.id.nav_products) {
            fab.setVisibility(View.VISIBLE);
            invalidateOptionsMenu();
            navigationView.getMenu().getItem(1).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductTypeFragment(), PRODUCT_TAG).commit();

        } else if (id == R.id.nav_orders) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(2).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new OrderFragment()).commit();

        } else if (id == R.id.nav_members) {
            fab.setVisibility(View.VISIBLE);
            navigationView.getMenu().getItem(3).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ExistingUserFragment(), MEMBER_TAG).commit();

        } else if (id == R.id.nav_settings) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(4).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SettingsFragment()).commit();

        } else if (id == R.id.broadcast) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(5).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new BroadcastMessage()).commit();

        } else if(id==R.id.contact_us){
            navigationView.getMenu().getItem(6).setChecked(true);
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction.  We also want to remove any currently showing
            // dialog, so make our own transaction and take care of that here.
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            FragmentManager fm = this.getSupportFragmentManager();
            ContactUsFragment newFragment = new ContactUsFragment();
            newFragment.show(fm, "dialog");


        } else if(id==R.id.nav_faqs){
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(7).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new FAQFragment()).commit();


        }else if(id==R.id.nav_terms_and_conditions){
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(8).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new TermsAndConditionsFragment()).commit();


        } else if(id==R.id.nav_about_us) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(9).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new AboutUsFragment()).commit();


        } else if (id == R.id.nav_logout) {
            if (Master.isNetworkAvailable(DashboardActivity.this)) {
                navigationView.getMenu().getItem(10).setChecked(true);

                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("token", Master.getToken());
                } catch (JSONException ignored) {
                }
                new GCMDeregisterTask(DashboardActivity.this).execute(jsonObject);

            } else {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_alertdialog_cannot_log_out_without_internet), getString(R.string.label_alertdialog_ok));
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void itemSelected(int itemIndex) {
        navigationView.getMenu().getItem(itemIndex).setChecked(true);
        if (itemIndex == 1 || itemIndex == 3)
            fab.setVisibility(View.VISIBLE);
        else
            fab.setVisibility(View.GONE);
    }


    @Override
    public void onCardClickListener(int position, int cat, Object obj) {
        args.clear();
        if (cat == Master.savedToProcessedKey) {
            PlacedOrderFragment placedOrderFragment = (PlacedOrderFragment) obj;
            placedOrderFragment.clickListener(position);
        } else if (cat == Master.cancelledOrderKey) {
            CancelledOrderFragment cancelledOrderFragment = (CancelledOrderFragment) obj;
            cancelledOrderFragment.clickListener(position);
        } else if (cat == Master.existingUserClickKey) {
            ExistingUserFragment existingUserFragment = (ExistingUserFragment) obj;
            existingUserFragment.clickListener(position);
        } else if (cat == Master.pendingUserRequestKey) {
            ExistingUserFragment existingUserFragment = (ExistingUserFragment) obj;
            existingUserFragment.clickListener(position);
        } else if (cat == Master.processedOrderKey) {
            ProcessedOrderFragment processedOrderFragment = (ProcessedOrderFragment) obj;
            processedOrderFragment.clickListener(position);
        }else if (cat == Master.deliveredOrderKey) {
            DeliveredOrderFragment deliveredOrderFragment = (DeliveredOrderFragment) obj;
            deliveredOrderFragment.clickListener(position);
        }else if (cat == Master.productTypeClickKey) {
            ProductTypeFragment productTypeFragment = (ProductTypeFragment) obj;
            productTypeFragment.clickListener(position);
        }

    }

    @Override
    public void onCardLongClickListener(final int position, int category)
    {

    }



    class GCMDeregisterTask extends AsyncTask<JSONObject, Void, String> {

        final Context context;

        GCMDeregisterTask(Context context) {
            this.context = context;
        }

        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getDeregisterTokenURL(), params[0], "POST", false, null, null);
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);

            if(!str.equals("exception"))
            {
                try {
                    JSONObject response = new JSONObject(str);
                    if(response.getString("response").equals("success"))
                    {

                        editor.putBoolean("login", false);
                        editor.putBoolean("logout", true);
                        editor.commit();

                        startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }
    }


    public static void updateSearchAdapter() {
        if (Master.productTypeSearchList != null) {
            products = new ArrayList<>();
            for (int i = 0; i < Master.productTypeSearchList.size(); ++i) {
                for (int j = 0; j < Master.productTypeSearchList.get(i).productItems.size(); ++j) {
                    products.add(Master.productTypeSearchList.get(i).productItems.get(j).getName());
                }
                if(itemsAdapter != null)
                    itemsAdapter.notifyDataSetChanged();
            }
        }
    }

    private int getInformationFromName(String name)
    {
        for(int i = 0; i < Master.productTypeSearchList.size(); ++i)
        {
            for(int j = 0; j < Master.productTypeSearchList.get(i).productItems.size(); ++j)
            {
                if(Master.productTypeSearchList.get(i).productItems.get(j).getName().equals(name))
                {
                    productName = Master.productTypeSearchList.get(i).productItems.get(j).getName();
                    productDescription=Master.productTypeSearchList.get(i).productItems.get(j).getDescription();
                    productUnitPrice=Master.productTypeSearchList.get(i).productItems.get(j).getUnitPrice();
                    stockQuantity=Master.productTypeSearchList.get(i).productItems.get(j).getStockQuantity();
                    productId=Master.productTypeSearchList.get(i).productItems.get(j).getID();
                    productPos=j;
                    productTypePos=i;
                    return j;
                }
            }
        }
        return -1;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        products = new ArrayList<>();
        itemsAdapter = new ArrayAdapter<>(this, R.layout.search_list_item, R.id.product_name, products);
        listView = (ListView) findViewById(R.id.search_list_view);
        updateSearchAdapter();
        listView.setAdapter(itemsAdapter);
        getMenuInflater().inflate(R.menu.options_menu,menu);

        MenuItem bSearch = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = bSearch;
        final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        //When clicked on back button after search, replaces frame with product type list
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                FrameLayout layout = (FrameLayout)findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout)findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);

                fab.setVisibility(View.VISIBLE);

                return true;
            }
        });

        //when you change  in query field
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }



            @Override
            public boolean onQueryTextChange(String newText) {

                query_entered=newText;

                if (TextUtils.isEmpty(newText)) {
                    listView.clearTextFilter();
                    itemsAdapter.getFilter().filter("");
                    //if query is cleared list view should show all suggestions which is in itemsadapter
                    listView.setAdapter(itemsAdapter);
                } else {

                    //code to display filtered results based on query entered

                    //clear the list view before everytime new query entered
                    listView.setAdapter(null);

                    //now update the listView based on entered query
                    //always create a new adapter(initialized to null) when new query is entered and update it and add it to listView
                    // now populate the list view with only set of items that start with characters in query entered
                    //to obtain this loop over all items and get the required items to add to listView
                    newProducts=new ArrayList<>();
                    ArrayAdapter<String> newitemsAdapter = new ArrayAdapter<>(DashboardActivity.this, R.layout.search_list_item, R.id.product_name, newProducts);
                    for(int i = 0; i < products.size(); i++) {
                        String item = products.get(i).toLowerCase();
                        String qr = newText.toLowerCase();

                        if(item.contains(qr)) {
                            newitemsAdapter.add( item );
                        }
                    }

                    //now update list view with new adapter
                    listView.setAdapter(newitemsAdapter);

                }

                return true;
            }
        });

        //to collapse query field when not in use
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if(!queryTextFocused) {
                    searchMenuItem.collapseActionView();
                    searchView.setQuery("", false);
                }
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //when clicked on search results,it should redirect to that particular product page

                //write a code here to map position to correct position in product list

                //get the value of query and store the original postions of search results

                ArrayList<Integer> newPosition=new ArrayList<>();
                for(int i = 0; i < products.size(); i++) {
                    String item = products.get(i).toLowerCase();
                    String qr = query_entered.toLowerCase();

                    if(item.contains(qr)) {
                        newPosition.add(i);
                    }
                }

                //now update the position to correct value stored previously
                position=newPosition.get(position);

                // TODO Auto-generated method stub
                Toast.makeText(DashboardActivity.this, products.get(position), Toast.LENGTH_SHORT).show();

                FrameLayout layout = (FrameLayout) findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);


                //to close search menu when clicked on back from product details page
                searchMenuItem.collapseActionView();
                searchView.setQuery("", false);

                getInformationFromName(products.get(position));
                Intent i = new Intent(DashboardActivity.this, EditProductActivity.class);
                i.putExtra("product_id",productId);
                i.putExtra("product_name", productName);
                i.putExtra("description",productDescription);
                i.putExtra("unitPrice",productUnitPrice);
                i.putExtra("quantity", stockQuantity);
                i.putExtra("fromWhere","fromDashboardActivity");
                i.putExtra("product_type_pos",productTypePos);
                i.putExtra("product_pos",productPos);
                startActivity(i);
            }
        });
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        ProductTypeFragment productTypeFragment = (ProductTypeFragment) getSupportFragmentManager().findFragmentByTag(PRODUCT_TAG);

        if(Master.isNetworkAvailable(DashboardActivity.this)) {
            switch (item.getItemId()) {
                case R.id.bProductTypeEnableDisable:
                    productTypeFragment.ProductTypeFunctions("EnableDisable");
                    return true;
                case R.id.bProductTypeDelete:
                    productTypeFragment.ProductTypeFunctions("Delete");
                    return true;
                case R.id.bProductTypeEdit:
                    productTypeFragment.ProductTypeFunctions("Edit");
                    return true;
                case R.id.bProductTypeUpward:
                    productTypeFragment.ProductTypeFunctions("Up");
                    fab.setVisibility(View.VISIBLE);
                    return true;
                case R.id.bProductTypeDonward:
                    productTypeFragment.ProductTypeFunctions("Down");
                    fab.setVisibility(View.VISIBLE);
                    return true;
                case R.id.search:
                    FrameLayout layout = (FrameLayout)findViewById(R.id.content_frame);
                    layout.setVisibility(View.GONE);
                    FrameLayout layout2 = (FrameLayout)findViewById(R.id.search_framelayout);
                    layout2.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.GONE);
                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }else{
            Toast.makeText(DashboardActivity.this,R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
            return true;
        }


    }

    public void updateStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#01b2ab"));
        }
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006b67")));
    }



    //For permissions at run time
    public static class CheckPermissions{
        public static void reqPerm(Context context, Activity activity) {
            // BEGIN_INCLUDE(camera_permission)
            // Check if the Camera permission is already available.

            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)+ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)+ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                // Camera permission has not been granted.
                //requestCameraPermission(activity);
                requestAllPermissions(activity);

            }



        }

        private static void requestAllPermissions(Activity activity){

            if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.RECORD_AUDIO)
                        ||ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.WRITE_EXTERNAL_STORAGE)){

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
            else{
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        }


    }


}
