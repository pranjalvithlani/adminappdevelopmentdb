package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Random;

import admin.lokacart.ict.mobile.com.adminapp.util.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.util.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.util.UploadImageDisplay;
import admin.lokacart.ict.mobile.com.adminapp.util.Validation;

/**
 * Created by Vishesh on 29-02-2016.
 */

public class GeneralSettingsFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback{

    private View generalSettingsFragmentView;
    private View editMinimumBillAmount;
    private ProgressDialog pd;
    private JSONObject responseObject;
    private TextView tMinimumBillAmount;
    private TextView tDescriptionCharRemaining;
    private EditText eMinimumBillAmount;
    private AlertDialog changeMinimumBillAmount;
    private Button bPositive;
    private Button bNegative;
    private String sMinimumBillAmount;
    private String sAmount;
    private String organisationDescription="";
    private String address="";
    private String mobileNumber="";
    private final int REQUEST_GALLERY = 2;
    private final int REQUEST_CAMERA = 1;
    private String tempImage;
    private Random randomno;
    private EditText eOrganisationDescription;
    private EditText eAddress;
    private EditText eMobilenUmber;
    private static final String TAG = "GeneralSettingsFragment";

    private SwitchCompat sStockManagement;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sMinimumBillAmount=getString(R.string.label_minimum_bill_amount);
        randomno = new Random();


        generalSettingsFragmentView = inflater.inflate(R.layout.fragment_general_settings, container, false);

        return generalSettingsFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tMinimumBillAmount=(TextView)generalSettingsFragmentView.findViewById(R.id.tMinimumBillAmount);
        TextView tAboutOrganisation = (TextView) generalSettingsFragmentView.findViewById(R.id.tAboutOrganisation);
        sStockManagement = (SwitchCompat) generalSettingsFragmentView.findViewById(R.id.sStockManagement);
        sStockManagement.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));

        if(getActivity()!=null && Master.isNetworkAvailable(getActivity())) {
            new GetGeneralSettingsTask(1).execute();
        }else
        {
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            sAmount="0";
            tMinimumBillAmount.setText(sMinimumBillAmount+sAmount);
        }

        tAboutOrganisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Master.isNetworkAvailable(getActivity())) {
                    new GetGeneralSettingsTask(2).execute();
                } else {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                }


            }
        });


        tMinimumBillAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                builder.setCancelable(false);
                editMinimumBillAmount=getActivity().getLayoutInflater().inflate(R.layout.change_min_bill_amount,null);
                builder.setView(editMinimumBillAmount);
                builder.setTitle(R.string.builder_title_change_minimum_bill_amount);
                builder.setPositiveButton(R.string.label_button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.setNegativeButton(R.string.label_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                eMinimumBillAmount=(EditText) editMinimumBillAmount.findViewById(R.id.eMinimumBillAmount);

                eMinimumBillAmount.setText(sAmount);

                changeMinimumBillAmount=builder.create();
                changeMinimumBillAmount.show();

                bPositive=changeMinimumBillAmount.getButton(AlertDialog.BUTTON_POSITIVE);
                bNegative=changeMinimumBillAmount.getButton(AlertDialog.BUTTON_NEGATIVE);

                bPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(!sAmount.equals(eMinimumBillAmount.getText().toString())) {

                            sAmount=eMinimumBillAmount.getText().toString();

                            if (getActivity() != null && Master.isNetworkAvailable(getActivity()))
                            {
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("orderThreshold", eMinimumBillAmount.getText().toString().trim());
                                    new UpdateThresholdTask().execute(jsonObject);
                                } catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }
                            }

                            else
                            {
                                Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                            }
                        }else{
                            Toast.makeText(getActivity(),getString(R.string.label_toast_no_changes_made),Toast.LENGTH_SHORT).show();
                            changeMinimumBillAmount.dismiss();

                        }

                    }
                });

                bNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeMinimumBillAmount.dismiss();
                    }
                });

            }

        });



        sStockManagement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (getActivity() != null && Master.isNetworkAvailable(getActivity()) && sStockManagement.isPressed()) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("stockManagement", sStockManagement.isChecked());
                        jsonObject.put("autoApprove", true);
                        new UpdateGeneralSettingsTask().execute(jsonObject);
                    } catch (JSONException ignored) {
                    }
                } else if(sStockManagement.isPressed()) {
                    Toast.makeText(getActivity(), getString(R.string.label_toast_Please_check_internet_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });


    }







    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(getActivity(), eOrganisationDescription)) ret = false;
        if (Validation.isMobileNumber(getActivity(), eMobilenUmber))ret=false;
        if (!Validation.hasText(getActivity(), eAddress)) ret = false;
        return ret;
    }



    // Methods for granting permission at run time!



    private void reqPerm() {
        // BEGIN_INCLUDE(camera_permission)
        // Check if the Camera permission is already available.


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

            requestCameraPermission();

        }
        else if(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestStoragePermission();
        }
        else {

            // Camera permissions is already available, show the camera preview.
            uploadImage();
        }
        // END_INCLUDE(camera_permission)

    }


    private void requestCameraPermission() {

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i(TAG,
                    "Displaying camera permission rationale to provide additional context.");
            /*Snackbar.make(mLayout, "Camera permission is needed to show the camera preview.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();*/

            //Snackbar method could also be used at the start of the app

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }
    private void requestStoragePermission() {
        Log.i(TAG, "Storage permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        int REQUEST_STORAGE = 0;
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
        // END_INCLUDE(camera_permission_request)
    }




    private void uploadImage()
    {
        final CharSequence[] options = { getString(R.string.upload_image_take_photo), getString(R.string.upload_image_choose_from_gallery),getString(R.string.upload_image_cancel) };
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.upload_image_title);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    tempImage = "temp" + randomno.nextInt(1000000) + ".jpg";
                    File f = new File(Environment.getExternalStorageDirectory(), tempImage);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/jpg");
                    startActivityForResult(intent, REQUEST_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        int RESULT_OK = -1;
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }

                try {

                    String filePath = f.getAbsolutePath();

                    Intent intent = new Intent(getContext(), UploadImageDisplay.class);
                    intent.putExtra("imgpath", filePath);
                    intent.putExtra("TAG", TAG);
                    startActivity(intent);




                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else if (requestCode == REQUEST_GALLERY)
            {
                final Uri selectedImageURI = data.getData();
                if(selectedImageURI != null) {
                    try {
                        String selectedImageFilePath = Master.getPath(getActivity(), selectedImageURI);

                        Intent intent = new Intent(getContext(),UploadImageDisplay.class);
                        intent.putExtra("imgpath", selectedImageFilePath);
                        intent.putExtra("TAG", TAG);
                        startActivity(intent);

                    } catch (Exception e) {
                        Toast.makeText(getActivity(), getString(R.string.label_toast_unable_to_get_the_file_path), Toast.LENGTH_LONG).show();
                    }
                }
            }

        }
    }





    public class GetGeneralSettingsTask extends AsyncTask<String, String, String>
    {
        final int cat;
        public GetGeneralSettingsTask(int cat)
        {
            this.cat=cat;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getGeneralSettingsURL(AdminDetails.getAbbr(), AdminDetails.getMobileNumber()),
                    null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());

        }

        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(GeneralSettingsFragment.this.isAdded()){

                if(s.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));

                }
                else
                {
                    try
                    {
                        responseObject = new JSONObject(s);

                        if(responseObject.get("response").equals("success"))
                        {

                            if(cat==1)
                            {
                                Boolean stockManagement = responseObject.getBoolean("stockManagement");
                                sStockManagement.setChecked(stockManagement);
                                sAmount=responseObject.getString("orderThreshold");
                                tMinimumBillAmount.setText(sMinimumBillAmount + sAmount);
                            }
                            else
                            {
                                organisationDescription=responseObject.getString("description");
                                address=responseObject.getString("address");
                                mobileNumber=responseObject.getString("phone");


                                init();
                            }


                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();

                        sAmount="0";
                        organisationDescription=getString(R.string.label_no_description);
                    }

                }

            }

        }
    }



    private void init()
    {
        final Dialog dialog=new Dialog(getActivity());
        dialog.setContentView(R.layout.about_organisation);
        dialog.setTitle(R.string.dialog_title_modify_organisation_details);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        eOrganisationDescription=(EditText) dialog.findViewById(R.id.eOrganisationDescription);
        eAddress=(EditText) dialog.findViewById(R.id.address);
        eMobilenUmber=(EditText) dialog.findViewById(R.id.contact2);
        tDescriptionCharRemaining = (TextView) dialog.findViewById(R.id.tDescriptionCharRemaining);
        tDescriptionCharRemaining.setText((500 - organisationDescription.length()) + " " +  getString(R.string.textview_characters_left));
        eOrganisationDescription.setText(organisationDescription);
        eOrganisationDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    try {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        eAddress.setText(address);
        eMobilenUmber.setText(mobileNumber);

        eOrganisationDescription.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    tDescriptionCharRemaining.setText((500 - s.length()) + " " +  getString(R.string.textview_characters_left));
            }
        });

        Button bUploadLogo = (Button) dialog.findViewById(R.id.bUploadLogo);
        Button bOrganisationDescriptionCancel = (Button) dialog.findViewById(R.id.bOrganisationDescriptionCancel);
        Button bSaveSettings = (Button) dialog.findViewById(R.id.bSaveSettings);

        bOrganisationDescriptionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bUploadLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reqPerm();
            }
        });

        bSaveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (organisationDescription.equals(eOrganisationDescription.getText().toString().trim())
                        && address.equals(eAddress.getText().toString().trim())
                        && mobileNumber.equals(eMobilenUmber.getText().toString().trim()))
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                else if (checkValidation()) {

                    if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                    {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("description", eOrganisationDescription.getText().toString().trim());
                            jsonObject.put("phone", eMobilenUmber.getText().toString().trim());

                            jsonObject.put("address", eAddress.getText().toString().trim());

                            new UploadOrganisationDescriptionTask().execute(jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getActivity(), R.string.label_toast_Form_contains_error, Toast.LENGTH_LONG).show();

                }
            }


        });

    }

    private class UpdateGeneralSettingsTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getGeneralSettingsUpdateURL(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }

        @Override
        protected void onPostExecute(String s) {
            if(pd != null && pd.isShowing()) {
                pd.dismiss();
            }

            if(s.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));

            }
            else
            {
                try
                {
                    responseObject = new JSONObject(s);
                    if(responseObject.get("response").equals("Successfully updated"))
                    {
                        Toast.makeText(getActivity(), getString(R.string.label_toast_changes_saved_successfully), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), Toast.LENGTH_SHORT).show();
                    }
                }
                catch(JSONException e){
                    Toast.makeText(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), Toast.LENGTH_SHORT).show();
                }
               /* catch (Exception e)
                {
                    Toast.makeText(getActivity(), getString(R.string.label_toast_something_went_worng), Toast.LENGTH_SHORT).show();
                }*/
            }

        }
    }


    private class UpdateThresholdTask extends AsyncTask<JSONObject,String,String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getThresholdUpdate(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }
        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(s.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));

            }
            else
            {

                try
                {
                    responseObject = new JSONObject(s);
                    if(responseObject.get("response").equals("Successfully updated"))
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_changes_saved_successfully, Toast.LENGTH_SHORT).show();
                        tMinimumBillAmount.setText(sMinimumBillAmount + eMinimumBillAmount.getText().toString().trim());
                        changeMinimumBillAmount.dismiss();
                    }
                    else if(responseObject.get("response").equals("timeout")){
                        Toast.makeText(getActivity(), "Timeout uploading image", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                    }
                }
                catch(JSONException e){
                    Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                }
            /*catch (Exception e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }*/


            }


        }
    }

    //---------------------------------------Upload Organisation Description API--------------------------------------------

    private class UploadOrganisationDescriptionTask extends AsyncTask<JSONObject,String,String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = GetJSON.getInstance();
            return getJSON.getJSONFromUrl(Master.getGeneralSettingsOrganisationDescriptionURL(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        }
        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(s.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), getString(R.string.label_alertdialog_ok));

            }
            else
            {
                try
                {
                    responseObject = new JSONObject(s);
                    if(responseObject.get("response").equals("Success"))
                    {
                        Toast.makeText(getActivity(), getString(R.string.label_toast_changes_saved_successfully), Toast.LENGTH_SHORT).show();
                        organisationDescription=eOrganisationDescription.getText().toString().trim();

                    }
                    else
                    {
                        Toast.makeText(getActivity(), getString(R.string.label_we_are_facing_some_technical_problems), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(getActivity(), R.string.label_we_are_facing_some_technical_problems, Toast.LENGTH_SHORT).show();
                }
                /*catch(Exception e){
                    Toast.makeText(getActivity(), getString(R.string.label_toast_something_went_worng), Toast.LENGTH_SHORT).show();

                }*/

            }


        }
    }

}
