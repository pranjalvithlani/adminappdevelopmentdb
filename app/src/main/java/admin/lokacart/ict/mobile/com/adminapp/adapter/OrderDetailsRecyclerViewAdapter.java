package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class OrderDetailsRecyclerViewAdapter extends RecyclerView.Adapter<OrderDetailsRecyclerViewAdapter.DataObjectHolder>
{

    private List<String[]> orderDetailsList = new ArrayList<>();

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        final TextView quantityName;
        final TextView rate;
        final TextView quantity;
        public DataObjectHolder(View itemView)
        {
            super(itemView);
            quantityName = (TextView) itemView.findViewById(R.id.quantityName);
            rate = (TextView) itemView.findViewById(R.id.rate);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
        }
    }

    public OrderDetailsRecyclerViewAdapter(ArrayList<String[]> myDataset)
    {
        orderDetailsList = myDataset;

    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_details_list_item_card_view, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        String[] list = orderDetailsList.get(position);
        holder.quantity.setText(list[2]);
        holder.rate.setText(list[1]);
        holder.quantityName.setText(list[0]);

    }

    @Override
    public int getItemCount() {
        return orderDetailsList.size();
    }

}
