package admin.lokacart.ict.mobile.com.adminapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 2/7/16.
 */
public class EditPlacedOrderAdapter extends RecyclerView.Adapter<EditPlacedOrderAdapter.DataObjectHolder>{

    private final Context context;
    private final TextView orderTotal;
    private final String stockEnabledStatus;

    public EditPlacedOrderAdapter(Context context,TextView orderTotal, String stockEnabledStatus){

        this.context=context;
        this.orderTotal=orderTotal;
        this.stockEnabledStatus=stockEnabledStatus;
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        final TextView tProductName;
        final TextView tPrice;
        final TextView tItemTotal;
        final TextView tStockStatus;
        final ImageButton bPlus;
        final ImageButton bMinus;
        final EditText eQuantity;


        final MyCustomEditTextListener myCustomEditTextListener;

        public DataObjectHolder(final View itemView, MyCustomEditTextListener myCustomEditTextListener)
        {
            super(itemView);
            tProductName = (TextView) itemView.findViewById(R.id.tCartProductName);
            tPrice = (TextView) itemView.findViewById(R.id.tPrice);
            tItemTotal = (TextView) itemView.findViewById(R.id.tItemTotal);
            tStockStatus = (TextView) itemView.findViewById(R.id.tStockStatus);
            bPlus = (ImageButton) itemView.findViewById(R.id.bPlus);
            bMinus = (ImageButton) itemView.findViewById(R.id.bMinus);
            eQuantity = (EditText) itemView.findViewById(R.id.eQuantity);
            this.myCustomEditTextListener = myCustomEditTextListener;
        }


    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_edit_cart, parent, false);
        return new DataObjectHolder(cardView, new MyCustomEditTextListener());
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {


        holder.eQuantity.addTextChangedListener(holder.myCustomEditTextListener);
        holder.myCustomEditTextListener.updatePosition(position,orderTotal,holder.tItemTotal, holder.eQuantity,
                holder.bPlus, holder.bMinus, holder.tStockStatus);
        holder.tProductName.setText("" + Master.editOrderList.get(position).getName());
        holder.tPrice.setText("\u20B9" + Master.editOrderList.get(position).getUnitPrice());



        holder.tItemTotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());
        if(Master.editOrderList.get(position).getQuantity() < 1)
        {
            holder.bMinus.setEnabled(false);
            holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
        }

        holder.bPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sqty = holder.eQuantity.getText().toString().trim();
                int dqty;
                if(sqty.equals(""))
                    dqty = 1;
                else
                    dqty = Integer.parseInt(sqty) + 1;


                if(dqty >= 999)
                {
                    holder.eQuantity.setText("999");
                    holder.bPlus.setEnabled(false);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                }
                else if(dqty >= Master.changeCheckList.get(position) && holder.tStockStatus != null &&
                        holder.tStockStatus.getVisibility() == View.VISIBLE &&
                        holder.tStockStatus.getText().toString().equals("OUT OF STOCK"))
                {
                    holder.bPlus.setEnabled(false);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    holder.eQuantity.setText("" + Master.changeCheckList.get(position));
                }
                else
                {
                    holder.bPlus.setEnabled(true);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                    holder.eQuantity.setText("" + dqty);
                }

                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            }
        });

        holder.bMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sqty = holder.eQuantity.getText().toString().trim();

                int dqty = Integer.parseInt(sqty);

                if(dqty > 1)
                    holder.eQuantity.setText("" + --dqty);

                else if(dqty == 1)
                {
                    holder.eQuantity.setText("");
                    holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    holder.bMinus.setEnabled(false);
                }

            }
        });




        if(stockEnabledStatus.equals("true"))
        {
            holder.tStockStatus.setVisibility(View.VISIBLE);
            holder.eQuantity.setEnabled(true);
            if(Master.editOrderList.get(position).getStockQuantity() !=0 )
            {
                holder.tStockStatus.setText("STOCK AVAILABLE");

                holder.bPlus.setEnabled(true);
                holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);


                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                holder.tStockStatus.setTextColor(Color.parseColor("#42A462"));


            }
            else
            {

                holder.tStockStatus.setText("OUT OF STOCK");
                holder.bPlus.setEnabled(false);
                holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                holder.tStockStatus.setTextColor(Color.RED);
            }


            if(Master.editOrderList.get(position).getQuantity() == 1)
            {
                holder.bMinus.setEnabled(false);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
            }
            else
            {
                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            }

        }
        else
        {
            holder.tStockStatus.setVisibility(View.GONE);
            holder.tStockStatus.setText("Status");
            holder.eQuantity.setEnabled(true);
        }

        holder.eQuantity.setText("" + Master.editOrderList.get(position).getQuantity());

    }

    @Override
    public int getItemCount() {
        return Master.editOrderList.size();
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        TextView itotal;
        EditText eqty;
        TextView cartTotal, tStockStatus;
        ImageButton bPlus, bMinus;

        public void updatePosition(int position,TextView cartTotal,TextView textView,EditText editText, ImageButton bPlus,
                                   ImageButton bMinus, TextView tStockStatus) {
            this.position = position;
            itotal=textView;
            eqty=editText;
            this.bPlus = bPlus;
            this.bMinus = bMinus;
            this.cartTotal=cartTotal;
            this.tStockStatus = tStockStatus;
        }



        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            try
            {

                double sum ;
                if (!editable.toString().equals("")) {
                    String total;
                    try {

                        bMinus.setEnabled(true);
                        bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);


                        if(Integer.parseInt(editable.toString()) > 999)
                        {
                            eqty.setText("999");
                            bPlus.setEnabled(false);
                            bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                            total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * 999);
                            Master.editOrderList.get(position).setQuantity(999);

                        }
                        else if(Integer.parseInt(editable.toString()) == 0)
                        {
                            bMinus.setEnabled(false);
                            bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                            bPlus.setEnabled(true);
                            bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                            total= String.format("%.2f",Master.editOrderList.get(position).getUnitPrice() * 0);
                            Master.editOrderList.get(position).setQuantity(0);
                        }
                        else if(Integer.parseInt(editable.toString()) == Master.changeCheckList.get(position) && tStockStatus != null &&
                                tStockStatus.getVisibility() == View.VISIBLE && tStockStatus.getText().toString().equals("OUT OF STOCK"))
                        {
                            bPlus.setEnabled(false);
                            bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                            total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Master.changeCheckList.get(position));
                            Master.editOrderList.get(position).setQuantity(Master.changeCheckList.get(position));
                        }
                        else if(Integer.parseInt(editable.toString()) > Master.changeCheckList.get(position) && tStockStatus != null &&
                                tStockStatus.getVisibility() == View.VISIBLE && tStockStatus.getText().toString().equals("OUT OF STOCK"))
                        {
                            bPlus.setEnabled(false);
                            bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                            eqty.setText("" + Master.changeCheckList.get(position));
                            total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Master.changeCheckList.get(position));
                            Master.editOrderList.get(position).setQuantity(Master.changeCheckList.get(position));

                        }
                        else
                        {
                            total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Integer.parseInt(editable.toString()));
                            Master.editOrderList.get(position).setQuantity(Integer.parseInt(editable.toString()));
                            bPlus.setEnabled(true);
                            bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                        }

                        Master.editOrderList.get(position).setTotal(Double.parseDouble(total));

                        itotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());
                        sum =0.0;
                        for(int i=0;i<Master.editOrderList.size();i++)
                        {
                            sum = sum +Master.editOrderList.get(i).getTotal();
                        }

                        cartTotal.setText("Total: " + String.format("%.2f", sum));

                    }
                    catch (NumberFormatException ignored) {
                    }
                }
                else
                {

                    Master.editOrderList.get(position).setQuantity(0);
                    Master.editOrderList.get(position).setTotal(0.0);

                    itotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());
                    sum =0.0;
                    for(int i=0;i<Master.editOrderList.size();i++)
                    {
                        sum = sum +Master.editOrderList.get(i).getTotal();
                    }

                    cartTotal.setText("Total: " + String.format("%.2f", sum));

                    bMinus.setEnabled(false);
                    bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);

                    bPlus.setEnabled(true);
                    bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                }

            }
            catch (Exception ignored)
            {

            }

        }
    }

}
