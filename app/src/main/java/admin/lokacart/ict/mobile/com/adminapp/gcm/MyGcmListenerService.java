package admin.lokacart.ict.mobile.com.adminapp.gcm;

/**
 * Created by SidRama on 15/01/16.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.gcm.GcmListenerService;

import admin.lokacart.ict.mobile.com.adminapp.util.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;

public class MyGcmListenerService extends GcmListenerService {


    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(("message"));
        String title = data.getString(("title"));
        int id = Integer.parseInt(data.getString("id"));

        if (id == 0) {
            sendNotification(message, title);


        }

        else if(id == 2)
        {
            Intent updateComplete = new Intent(Master.ACTION);
            updateComplete.putExtra("saved", data.getString("saved"));
            updateComplete.putExtra("processed", data.getString("processed"));
            updateComplete.putExtra("cancelled", data.getString("cancelled"));
            updateComplete.putExtra("totalUsers", data.getString("totalUsers"));
            updateComplete.putExtra("newUsersToday", data.getString("newUsersToday"));
            updateComplete.putExtra("pendingUsers", data.getString("pendingUsers"));
            updateComplete.putExtra("paid",data.getString("paid"));
            updateComplete.putExtra("unpaid",data.getString("unpaid"));
            updateComplete.putExtra("delivered",data.getString("delivered"));
            LocalBroadcastManager.getInstance(this).sendBroadcast(updateComplete);

        }
    }

    private void sendNotification(String message, String title) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_shopping_cart_white_36dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }





}