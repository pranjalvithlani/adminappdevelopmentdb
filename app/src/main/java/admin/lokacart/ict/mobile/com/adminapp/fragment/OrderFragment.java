package admin.lokacart.ict.mobile.com.adminapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import admin.lokacart.ict.mobile.com.adminapp.activity.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.adapter.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentManager;

import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by root on 19/1/16.
 */
public class OrderFragment extends Fragment {
    private ViewPager viewPager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        if(OrderFragment.this.isAdded() && getActivity()!=null) {
            ((DashboardActivity) getActivity()).updateStatusBarColor();
        }

        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
        getActivity().setTitle(R.string.title_orders);
        setRetainInstance(true);

        final TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_pending_order_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_processed_order_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_delivered_order_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_cancelled_order_fragment)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        FragmentManager fm = getFragmentManager();

        PagerAdapter mPageAdapter = new PagerAdapter(fm, tabLayout.getTabCount(), "Order");

        viewPager = (ViewPager)rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(0);

        try
        {
            Bundle bundle = this.getArguments();
            if(bundle.get("to").equals("processed"))
                viewPager.setCurrentItem(1);
            else if(bundle.get("to").equals("cancelled"))
                viewPager.setCurrentItem(3);
            else if(bundle.get("to").equals("delivered"))
                viewPager.setCurrentItem(2);
        }
        catch (Exception e)
        {
            viewPager.setCurrentItem(0);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                DashboardActivity.resetBackPress();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}

