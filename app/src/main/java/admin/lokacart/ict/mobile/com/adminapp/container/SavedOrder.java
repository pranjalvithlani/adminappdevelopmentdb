package admin.lokacart.ict.mobile.com.adminapp.container;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;

import admin.lokacart.ict.mobile.com.adminapp.util.Master;

public class SavedOrder implements Serializable{

    private String userName;
    private int orderId;
    private String timeStamp;
    private String comment;
    private TreeMap<String,String[]> placedOrderItemLists;
    private String payment;
    private TreeMap<String,String[]> itemList;
    private String delDate;

    public String getComment() {
        return comment;
    }
    public String getUserName() {
        return userName;
    }
    public int getOrderId() {
        return orderId;
    }
    public String getPayment() { return payment; }
    public void setPayment() { this.payment = "true"; }
    public String getDelDate(){
        return delDate;
    }
    public void setDelDate(String delDate){
        this.delDate = delDate;
    }

    public SavedOrder(JSONObject order, int pos, String type)
    {

        if(type.equals(Master.PLACEDORDER)){

            try {
                double totalBill = 0.0;
                this.orderId=order.getInt("orderid");
                this.userName = order.getString("username");
                this.timeStamp=order.getString("timestamp");

                if(order.has("delivery")) {
                    this.delDate = order.getString("delivery");
                }
                else{
                    this.delDate = "not found";
                }
                JSONArray orderItems=order.getJSONArray("items");
                this.placedOrderItemLists = new TreeMap<>();
                HashSet<String> set = new HashSet<>();
                for(int i=  0;i<orderItems.length();i++){
                    JSONObject item=(JSONObject)orderItems.get(i);
                    String name=item.getString("productname");
                    String[] temp=new String[3];
                    temp[0]=item.getString("rate");
                    int quantity = Integer.parseInt(item.getString("quantity"));
                    if(!set.contains(name))
                    {
                        set.add(name);
                        temp[1]=item.getString("quantity");
                    }
                    else
                    {
                        temp[1]=String.valueOf(Integer.parseInt(this.placedOrderItemLists.get(pos + name)[1]) + Integer.parseInt(item.getString("quantity")));
                    }

                    temp[2]=item.getString("stockQuantity");
                    this.placedOrderItemLists.put(pos + name, temp);
                    totalBill = totalBill +(Double.parseDouble(String.format("%.2f",(Double.parseDouble(temp[0])* quantity))));

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        else {
            try {
                this.orderId = order.getInt("orderid");
                this.userName = order.getString("username");
                this.timeStamp = order.getString("timestamp");

                 if(order.has("delivery")) {
                    this.delDate = order.getString("delivery");
                }
                else{
                    this.delDate = "not found";
                }

                try {
                    this.payment = order.getString("paid");
                } catch (JSONException ignored) {

                }

                try {
                    this.comment = order.getString("comment");
                } catch (JSONException e) {
                    this.comment = "No Comments";
                }
                JSONArray orderItems = order.getJSONArray("items");
                this.itemList = new TreeMap<>();
                for (int i = 0; i < orderItems.length(); i++) {
                    JSONObject item = (JSONObject) orderItems.get(i);
                    String name = item.getString("productname");
                    String[] temp = new String[2];
                    temp[0] = item.getString("rate");
                    temp[1] = item.getString("quantity");
                    this.itemList.put(pos + name, temp);
                }
            }
            catch (JSONException j){
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public String getTimeStamp() {
        return timeStamp;
    }



    public ArrayList<String[]> getItemsList(int pos)
    {
        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res;
        int posLen = (""+pos).length();
        for (String itemName:itemList.keySet())
        {
            res = new String[3] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]=itemList.get(itemName)[0];
            res[2]=itemList.get(itemName)[1];

            itemlist.add(res);
        }
        return itemlist;
    }

    public ArrayList<String[]> getPlacedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res ;

        int posLen = (""+pos).length();


        for (String itemName: placedOrderItemLists.keySet())
        {

            res = new String[5] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]= placedOrderItemLists.get(itemName)[0];
            res[2]= placedOrderItemLists.get(itemName)[1];

            double rate=Double.parseDouble(placedOrderItemLists.get(itemName)[0]);
            int quantity=Integer.parseInt(placedOrderItemLists.get(itemName)[1]);
            double total = Double.parseDouble(String.format("%.2f",(quantity*rate)));

            res[3]= ""+total;
            res[4]= placedOrderItemLists.get(itemName)[2]; //this is stock quantity



            itemlist.add(res);
        }

        return itemlist;
    }

}