package admin.lokacart.ict.mobile.com.adminapp.container;

/**
 * Created by madhav on 16/6/16.
 */

public class Product {

    private double unitPrice;
    private double total;
    private int quantity;
    private int stockQuantity;
    private String name;

    public String getID() {
        return ID;
    }

    private String ID;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    private String Description;

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product(String name, double unitPrice, int stockQuantity, String Description, String ID) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = 0.0;
        this.quantity = (int) (0.0 /unitPrice);
        this.stockQuantity = stockQuantity;
        this.Description = Description;
        this.ID=ID;
    }



    public Product(String name, int quantity, int stockQuantity,Double unitPrice, Double total) {
        this.name = name;
        this.quantity = quantity;
        this.stockQuantity=stockQuantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }




    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
